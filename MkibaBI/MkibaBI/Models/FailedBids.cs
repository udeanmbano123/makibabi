﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class FailedBids
    {
        public string Reason { get; set; }

        public string AmountPaid { get; set; }
      public string Date_Created { get; set; }
    }
}