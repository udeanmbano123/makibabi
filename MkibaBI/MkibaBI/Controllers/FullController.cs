﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MkibaBI.DAO;
using MkibaBI.DAO.security;
using WebMatrix.WebData;
using WebMatrix.Data;
using RestSharp;
using Newtonsoft.Json;
using MkibaBI.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using PagedList;

namespace MkibaBI.Controllers
{
    public class FullController : Controller
    {
        private Mkiba db2 = new Mkiba();
        // GET: Full
        public ActionResult Index()
        {
            try
            {

                int regcount = db2.Account_Creation.ToList().Where(a => a.Active == true).Count();

                int regcount2 = db2.Account_Creation.ToList().Where(a => a.Active == true && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

                int by = (from c in db2.Bond_Payment_Audit
                          select new { c.CDSC_Number }).Distinct().Count();

                var db = Database.Open("Mkiba2");

                var selectQueryString = "SELECT * from Bond_Payment_FailedBids";

                int fy = db.Query(selectQueryString).ToList().Count();

                var selectQueryString2 = "SELECT * from Account_Creation where TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_Audit) And TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_FailedBids)";

                int ny = db.Query(selectQueryString2).ToList().Count();
                double per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;

                ViewBag.RegCount = regcount.ToString("#,##0.00");
                ViewBag.By = by.ToString("#,##0.00");
                ViewBag.PC = Math.Round(per, 2).ToString();
                ViewBag.FY = fy.ToString("#,##0.00");
                ViewBag.NY = ny.ToString("#,##0.00");
                ViewBag.TY = regcount2.ToString("#,##0.00");

            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }

        public ActionResult Index2()
        {
            try
            {

                int potensafari = db2.Account_Creation.ToList().Where(a => a.MNO_ == "SAFARICOM" && a.Active == true).Count();

                int potenairtel = db2.Account_Creation.ToList().Where(a => a.MNO_ == "AIRTEL" && a.Active == true).Count();

                var db = Database.Open("Mkiba2");

                var selectQueryString = "SELECT * from Bond_Payment_FailedBids";
                int actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM'").ToList().Count();

                int actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL'").Count();

                ViewBag.PS = potensafari.ToString("#,##0.00");
                ViewBag.PA = potenairtel.ToString("#,##0.00");
                ViewBag.AS = actualsafari.ToString("#,##0.00");
                ViewBag.AA = actualairtel.ToString("#,##0.00");

                double c1 = (Convert.ToDouble(actualsafari) / Convert.ToDouble(potensafari)) * 100;
                double c2 = (Convert.ToDouble(actualairtel) / Convert.ToDouble(potenairtel)) * 100;

                ViewBag.CS = Math.Round(c1, 2).ToString();
                ViewBag.CA = Math.Round(c2, 2).ToString();
                int regcount = db2.Account_Creation.ToList().Where(a => a.Active == true).Count();
                string dt = DateTime.Now.ToString("dd/MM/yyyy");
                int regcount2 = db2.Account_Creation.ToList().Where(a => a.Active == true && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

                int by = (from c in db2.Bond_Payment_Audit
                          select new { c.CDSC_Number }).Distinct().Count();

                double per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;


                ViewBag.PC = Math.Round(per, 2).ToString();
                decimal? overall = db2.Bond_Payment_Audit.ToList().Sum(a => a.No_of_Notes_Applied);

                decimal? overallsafari = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "SAFARICOM").Sum(a => a.No_of_Notes_Applied);
                decimal? overallairtel = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "AIRTEL").Sum(a => a.No_of_Notes_Applied);

                decimal? overallt = db2.Bond_Payment_Audit.ToList().Where(a => a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);

                decimal? overallsafarit = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);
                decimal? overallairtelt = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "AIRTEL" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);

                ViewBag.OO = Convert.ToDouble(overall).ToString("#,##0.00");
                ViewBag.OS = Convert.ToDouble(overallsafari).ToString("#,##0.00");
                ViewBag.OA = Convert.ToDouble(overallairtel).ToString("#,##0.00");

                ViewBag.OOT = Convert.ToDouble(overallt).ToString("#,##0.00");
                ViewBag.OST = Convert.ToDouble(overallsafarit).ToString("#,##0.00");
                ViewBag.OAT = Convert.ToDouble(overallairtelt).ToString("#,##0.00");

            }
            catch (Exception)
            {


            }
            return View();
        }

        public ActionResult Index3()
        {
            try
            {

                int failedreg = db2.AccountC_FailedReg.ToList().Count();
                var db = Database.Open("Mkiba2");

                var fail = db.Query("select IsNull(COUNT(ReceiptNumber),'0') as 'CC' from Bond_Payment_FailedBids").ToList();
                var sf = db.Query("select sum(TRY_CONVERT(MONEY,AmountPaid)) as 'Total' from Bond_Payment_FailedBids").ToList();
                string vf = "";
                string ff = "";

                foreach (var d in sf)
                {
                    vf = d.Total.ToString();
                }
                foreach (var q in fail)
                {
                    ff = q.CC.ToString();
                }

                string comfR = "", Rerr = "";
                try
                {

                    var commonreg = db.Query("select * from CommonError").ToList();

                    foreach (var d in commonreg)
                    {
                        comfR = d.Reason.ToString();
                        Rerr = d.RError.ToString();
                    }
                }
                catch (Exception)
                {


                }

                string comfRB = "", RerrB = "";
                var commonbid = db.Query("select top 1 Reason,IsNull(Count(ReceiptNumber),'0') as 'RError' from Bond_Payment_FailedBids group by Reason order by Count(ReceiptNumber) desc").ToList();
                foreach (var d in commonbid)
                {
                    comfRB = d.Reason.ToString();
                    RerrB = d.RError.ToString();
                }
                decimal? avgR = 0;
                try
                {
                    avgR = db2.AccountC_FailedReg.ToList().Average(a => a.AmountPaid);

                }
                catch (Exception)
                {

                    avgR = 0;
                }
                try
                {
                    ViewBag.FailedReg = failedreg.ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.FailedReg = "0";
                }
                try
                {
                    ViewBag.FB = Math.Round(Convert.ToDecimal(ff), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.FB = "0";
                }
                ViewBag.CommonError = comfR.ToString();
                try
                {
                    ViewBag.CommonOccur = Convert.ToDouble(Rerr).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.CommonOccur = "0";
                }
                ViewBag.CommonErrorB = comfRB.ToString();
                try
                {
                    ViewBag.CommonOccurB = Convert.ToDouble(RerrB).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.CommonOccurB = "0";
                }
                try
                {
                    ViewBag.SF = Math.Round(Convert.ToDecimal(vf), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.SF = "0";
                }
                try
                {
                    ViewBag.AVR = Math.Round(Convert.ToDecimal(avgR), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.AVR = "0";
                }
                try
                {
                    //var sfv = db.Query("select IsNull(AVG(CAST(AmountPaid As money)),'0') As 'Total' from Bond_Payment_FailedBids").ToList();
                    //string vfv = "";
                    //foreach (var d in sfv)
                    //{
                    //    vfv = d.Total.ToString();
                    //}
                    //ViewBag.FAV = Math.Round(Convert.ToDecimal(vfv), 2).ToString();

                }
                catch (Exception)
                {

                    ViewBag.FAV = "0";

                }
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');

                //var sfc = db.Query("select IsNull(Reason,'None') As 'Reason',Date_Created,IsNull(AmountPaid,'0') As 'AmountPaid' from Bond_Payment_FailedBids").ToList();
                //var client = new RestClient(baseUrl);
                //var request = new RestRequest("failedbid", Method.GET);
                //IRestResponse response = client.Execute(request);
                // string validate = response.Content;
                List<FailedBids> dataList = falls();



                try
                {
                    ViewBag.SFP = dataList.ToList();
                }
                catch (Exception)
                {

                    ViewBag.SFP = null;
                }

            }
            catch (Exception)
            {


            }
            return View();
        }

        public List<FailedBids> falls()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids order by Date_Created desc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<FailedBids>();
            while (reader.Read())
            {
                var accountDetails = new FailedBids
                {

                    Reason = reader.GetValue(0).ToString(),
                    AmountPaid = reader.GetValue(1).ToString(),
                    Date_Created = Convert.ToDateTime(reader.GetValue(2)).ToString("dd/MM/yyyy:HH:mm:ss")
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public List<DailyTrade> Safari()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2017-03-23' as date) and CAST(a.Date_Created as date)<=CAST('2017-03-30' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public List<DailyTrade> Safari2(string dt, string dt2)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public List<DailyTrade> Airtel()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2017-03-23' as date) and CAST(a.Date_Created as date)<=CAST('2017-03-30' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public List<DailyTrade> Airtel2(string dt, string dt2)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public List<DailyTradeFull> Makiba()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit)/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit )/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Average Bid' ,(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2017-03-23' as date) and CAST(a.Date_Created as date)<=CAST('2017-03-30' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTradeFull>();
            while (reader.Read())
            {
                var accountDetails = new DailyTradeFull
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString()),

                    averagePer = Convert.ToDecimal(reader.GetValue(10).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        public List<DailyTradeFull> Makiba2(string dt, string dt2)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit)/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit )/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Average Bid' ,(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTradeFull>();
            while (reader.Read())
            {
                var accountDetails = new DailyTradeFull
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString()),

                    averagePer = Convert.ToDecimal(reader.GetValue(10).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        public ActionResult Index4()
        {

            return View();
        }
        public List<Investor> fallsz()
        {
            //return listEmp.First(e => e.ID == id); 
            string my = @"
select top 20 CDSC_Number as 'Account',Surname_CompanyName+' '+OtherNames As 'Name',MNO_ as 'MNO', (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) as 'Invested Amount' from Account_Creation order by (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) desc

                       ";
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = my;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Investor>();
            while (reader.Read())
            {
                var accountDetails = new Investor
                {
                    Account = reader.GetValue(0).ToString(),
                    Name = reader.GetValue(1).ToString(),

                    MNO = reader.GetValue(2).ToString(),

                    InvestedAmount = Convert.ToDecimal(reader.GetValue(3).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public List<AnalysisByBand> fallsno()
        {
            //return listEmp.First(e => e.ID == id); 

            string my = @"
select 'Minimum amount-Kshs. 3,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied<=3000) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied<=3000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied<=3000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied<=3000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Kshs. 3,001-10000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied>=3001 and No_of_Notes_Applied<=10000 ) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=3001 and No_of_Notes_Applied<=10000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied>=3001 and No_of_Notes_Applied<=10000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=3001 and No_of_Notes_Applied<=10000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Kshs. 10001-20000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied>=10001 and No_of_Notes_Applied<=20000) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=10001 and No_of_Notes_Applied<=20000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied>=10001 and No_of_Notes_Applied<=20000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=10001 and No_of_Notes_Applied<=20000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Kshs. 20001-50000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied>=20001 and No_of_Notes_Applied<=50000) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=20001 and No_of_Notes_Applied<=50000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied>=20001 and No_of_Notes_Applied<=50000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=20001 and No_of_Notes_Applied<=50000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Kshs. 50,001-100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied>=50001 and No_of_Notes_Applied<=100000) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=50001 and No_of_Notes_Applied<=100000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied>=50001 and No_of_Notes_Applied<=100000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>=50001 and No_of_Notes_Applied<=100000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Above Kshs.100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied>100000) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>100000))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit where No_of_Notes_Applied>100000) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied>100000))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'
union all
select 'Total Investors' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit) as 'Number',CAST( (((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit))/((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage',(select isnull( sum(No_of_Notes_Applied),'0.00') from Bond_Payment_Audit) as 'Value',CAST((((select cast(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit))/((select CAST(isnull( sum(No_of_Notes_Applied),'0.00') as decimal) from Bond_Payment_Audit)))*100 as decimal(18,2)) as 'Percentage Of Value'

";
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = my;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<AnalysisByBand>();
            while (reader.Read())
            {
                var accountDetails = new AnalysisByBand
                {
                    analysisband = reader.GetValue(0).ToString(),
                    Number = Convert.ToInt64(reader.GetValue(1).ToString()),

                    Percentage = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    Value = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    PercentageValue = Convert.ToDecimal(reader.GetValue(4).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public List<CumulativeBuys> CumulativeBu()
        {
            //return listEmp.First(e => e.ID == id); 
            string my = @"
--Cumulative Buys
select 'Total Buys Cumulative' as 'c', (select isnull(sum(AmountPaid),'0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_='SAFARICOM') As 'Safaricom',(select isnull(sum(AmountPaid),'0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_='AIRTEL') as 'Airtel',((select isnull(sum(AmountPaid),'0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_='SAFARICOM') + (select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL'))  as 'Total'
--Percentage of Bond
union all
select 'Percentage Of Bond Taken up' as 'c', ((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM') + (select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')))*100 As 'Safaricom',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM') + (select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')))*100 as 'Airtel', ((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM') + (select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')))*100 + ((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM') + (select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')))*100 as 'Total'
union all
--total registrations
select 'Total Registrations' as 'c',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM') As 'SAFARICOM',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL') As 'AIRTEL', (Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM')+(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL') as 'Totals'
--total no bids
union all
select 'Total No. Bids' as 'c',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') As 'Safaricom',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as 'Airtel',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM')+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as 'Totals'
union all
--total buyers
select 'Total Buyers' as 'c',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_='SAFARICOM') As 'SAFARICOM',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_='AIRTEL') as 'AIRTEL',(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_='SAFARICOM')+(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_='AIRTEL') As 'Totals'
--Percentage of Buyers to Registrations
union all
select 'Percentage of Buyers to Registrations' as 'c',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM')/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM'))*100 as decimal(18,2)) As 'SAFARICOM',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL')/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL'))*100 as decimal(18,2)) as 'AIRTEL', CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM')/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM'))*100 as decimal(18,2)) + CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL')/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL'))*100 as decimal(18,2)) as 'Totals'
union all
--Average Bid
select 'Average Bid' as 'c',CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') as decimal(18,2)) As 'Safaricom',CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as decimal(18,2)) as 'Airtel',CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') as decimal(18,2)) + CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as decimal(18,2))  as 'Totals'
union all
--Average Per Investor
select 'Average Per Investor' as 'c',CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/(Select distinct count(CDSC_Number) from Bond_Payment_Audit where MNO_='SAFARICOM') AS decimal(18,2)) As 'SAFARICOM',CAST((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL')/(Select distinct count(CDSC_Number) from Bond_Payment_Audit where MNO_='AIRTEL') AS decimal(18,2)) as 'AIRTEL',CAST((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM')/(Select distinct count(CDSC_Number) from Bond_Payment_Audit where MNO_='SAFARICOM') AS decimal(18,2)) + CAST((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL')/(Select distinct count(CDSC_Number) from Bond_Payment_Audit where MNO_='AIRTEL') AS decimal(18,2)) As 'Totals'
                      ";
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = my;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CumulativeBuys>();
            while (reader.Read())
            {
                var accountDetails = new CumulativeBuys
                {
                    c = reader.GetValue(0).ToString(),
                    Safaricom = Convert.ToDecimal(reader.GetValue(1).ToString()),

                    Airtel = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    Total = Convert.ToDecimal(reader.GetValue(3).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public ActionResult Index5()
        {

            return View();
        }

        public ActionResult Index6()
        {
            //Issued Quantity
            ViewBag.ps = db2.Bond_Payment_Audit.ToList().Sum(a => a.AmountPaid);

            var cum = CumulativeBu();


            //Safaricom Reconciliation
            ViewBag.sr = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "SAFARICOM").Sum(a => a.AmountPaid);
            //Airtel Reconciliattion
            ViewBag.ar = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "AIRTEL").Sum(a => a.AmountPaid);

            //
            ViewBag.analysis = fallsno();

            ViewBag.invs = fallsz();

            ViewBag.Cum = cum;
            return View();
        }

        public ActionResult Index7(string sortOrder, string Date1String, string Date2String, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Safari()
                      select s;
            if ((Date1String != null && Date2String != null))
            {
                per = Safari2(Date1String, Date2String);
            }
            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.BuysPerDay);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }
        public ActionResult Index8(string sortOrder, string Date1String, string Date2String, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Airtel()
                      select s;
            if ((Date1String != null && Date2String != null))
            {
                per = Airtel2(Date1String, Date2String);
            }
            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.BuysPerDay);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Index9(string sortOrder, string Date1String, string Date2String, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Makiba()
                      select s;
            if ((Date1String != null && Date2String != null))
            {
                per = Makiba2(Date1String, Date2String);
            }

            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.BuysPerDay);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }


        public ActionResult Unlock()
        {
            return View();
        }
        public ActionResult DownloadPdf()
        {

            return new Rotativa.ActionAsPdf("Index");
        }
    }
}