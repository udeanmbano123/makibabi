﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

using PagedList;
using TrackerEnabledDbContext.Common.Models;

using MkibaBI.DAO;

namespace MkibaBI.Controllers
{    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
     // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [DAO.security.CustomAuthorize(Roles = "Admin")]
    public class AuditLogController : Controller
    {
        private SBoardContext db = new SBoardContext();
        // GET: AuditLog
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";
            var audit = from s in db.AuditLog
                select s;
            foreach(var p in audit)
            {
                string s = p.UserName;
            }
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
               audit = audit.Where(s =>
                 s.UserName.ToUpper().Contains(searchString.ToUpper())
                 ||
                 s.UserName.ToUpper().Contains(searchString.ToUpper()));
            }

            else if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
               audit = from s in db.AuditLog
                          
                             where date <= s.EventDateUTC && date2 >= s.EventDateUTC
                             select s;


            }
            switch (sortOrder)
            {
                case "name_desc":
                    audit = audit.OrderByDescending(s => s.UserName);
                    break;

                case "Date":
                    audit = audit.OrderBy(s => s.EventDateUTC);
                    break;
                case "date_desc":
                    audit = audit.OrderByDescending(s => s.EventDateUTC);
                    break;
                default:
                 audit = audit.OrderBy(s => s.AuditLogId);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View( audit.ToPagedList(pageNumber, pageSize));
        }

        // GET: AuditLog/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
          AuditLog audit = await db.AuditLog.FindAsync(id);
            if (audit == null)
            {
                return HttpNotFound();
            }
            return View(audit);
        }


        // POST: AuditLog/Create
       


        // GET: AdminShares/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
          AuditLog audit = await db.AuditLog.FindAsync(id);
            if (audit == null)
            {
                return HttpNotFound();
            }
            return View(audit);
        }

        // POST: AdminShares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AuditLog audit = await db.AuditLog.FindAsync(id);
            db.AuditLog.Remove(audit);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
