﻿using MkibaBI.DAO.security;
using MkibaBI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MkibaBI.Controllers
{
    //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin,User")]
    // [CustomAuthorize(Users = "1")]
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Registration()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Include = "From,To")] Post registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/Registrations.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult Channel()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            return Redirect("~/Reporting/Channels.aspx");
        }
        public ActionResult PurchaseGender()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            return Redirect("~/Reporting/PurchaseGender.aspx");
        }

        public ActionResult PurchaseAge()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            return Redirect("~/Reporting/PurchaseAges.aspx");
        }
    }
}