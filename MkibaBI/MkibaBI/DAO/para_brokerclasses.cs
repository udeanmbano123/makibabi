//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class para_brokerclasses
    {
        public int ID { get; set; }
        public string Broker { get; set; }
        public string Class_name { get; set; }
        public string Class_code { get; set; }
        public string Description { get; set; }
    }
}
