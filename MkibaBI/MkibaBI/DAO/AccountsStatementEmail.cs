//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountsStatementEmail
    {
        public decimal ID_ { get; set; }
        public string CDS_Number { get; set; }
        public string EmailAddress { get; set; }
        public string EmailMessage { get; set; }
        public string StatementName { get; set; }
        public Nullable<System.DateTime> EnquiryDate { get; set; }
        public Nullable<int> SentFlag { get; set; }
    }
}
