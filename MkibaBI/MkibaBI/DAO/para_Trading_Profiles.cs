//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class para_Trading_Profiles
    {
        public decimal ProfileID { get; set; }
        public string ProfileName { get; set; }
        public string LoginName { get; set; }
        public string DealerCode { get; set; }
        public string TradingParticipant { get; set; }
        public string LoginStatus { get; set; }
        public Nullable<decimal> MsgSeqNum { get; set; }
        public string Role { get; set; }
        public string BrokerCode { get; set; }
        public string Target { get; set; }
    }
}
