//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_units_move
    {
        public int id { get; set; }
        public string fromcdsnumber { get; set; }
        public string tocdsnumber { get; set; }
        public Nullable<decimal> quantity { get; set; }
        public string status { get; set; }
        public string company { get; set; }
        public Nullable<int> deal { get; set; }
        public Nullable<decimal> total_charge { get; set; }
        public string charge_name { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> buy_amount { get; set; }
        public Nullable<decimal> Dealprice { get; set; }
        public string Language { get; set; }
        public string Mobile { get; set; }
        public string Mobile_buy { get; set; }
    }
}
