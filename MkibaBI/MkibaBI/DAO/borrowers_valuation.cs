//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class borrowers_valuation
    {
        public int id { get; set; }
        public string client_number { get; set; }
        public string BalanceSheetItems { get; set; }
        public string Class { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<decimal> Value { get; set; }
        public string Capturedby { get; set; }
    }
}
