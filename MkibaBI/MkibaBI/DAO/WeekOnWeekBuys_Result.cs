//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    
    public partial class WeekOnWeekBuys_Result
    {
        public string WeekEnding { get; set; }
        public int DisbCount { get; set; }
        public decimal DisbAmt { get; set; }
        public int RepayCount { get; set; }
        public decimal RepayAmt { get; set; }
        public string color { get; set; }
        public string Company { get; set; }
    }
}
