//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Borrowers_Reg
    {
        public int id { get; set; }
        public string client_type { get; set; }
        public string client_name { get; set; }
        public string client_number { get; set; }
        public string company { get; set; }
        public string securities { get; set; }
        public Nullable<decimal> unit_price { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string capturedby { get; set; }
        public string status { get; set; }
    }
}
