//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class settlement_movement
    {
        public int id { get; set; }
        public string company { get; set; }
        public string cds_number { get; set; }
        public string trans_type { get; set; }
        public Nullable<decimal> quantity { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string source { get; set; }
        public Nullable<decimal> uploadId { get; set; }
        public Nullable<decimal> UpdateState { get; set; }
        public Nullable<decimal> Order_Quantity { get; set; }
        public Nullable<System.DateTime> TplusDealDate { get; set; }
    }
}
