﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class DailyTrade
    {
      public DateTime Day { get; set; }
      public decimal TotalBuyC { get; set; }
      public decimal BuysPerDay { get; set; }
    
        public decimal percentageBond { get; set; }

        public long TotalRegistrations { get; set; }

        public long RegistrationsP { get; set; }

        public long totalBids { get; set; }

        public long totalBuyers { get; set; }

        public decimal BPR { get; set; }

        public decimal averageBid { get; set; }

    }
    public class DailyTradeFull
    {
        public DateTime Day { get; set; }
        public decimal TotalBuyC { get; set; }
        public decimal BuysPerDay { get; set; }

        public decimal percentageBond { get; set; }

        public long TotalRegistrations { get; set; }

        public long RegistrationsP { get; set; }

        public long totalBids { get; set; }

        public long totalBuyers { get; set; }

        public decimal BPR { get; set; }

        public decimal averageBid { get; set; }

        public decimal averagePer { get; set; }
    }
}