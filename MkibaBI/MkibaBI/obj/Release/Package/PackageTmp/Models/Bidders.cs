﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class Bidders
    {
        public string CDSC_Number { get; set; }
        public string ForeName { get; set; }
        public string Surname { get; set; }
       public string Fullnames { get; set; }
        public string Holding { get; set; }
        public string color { get; set; }
    }
}