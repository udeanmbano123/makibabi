﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class AnalysisByBand
    {
        public string analysisband { get; set; }
        public long Number { get; set; }

        public decimal Percentage { get; set; }

        public decimal Value { get; set; }

        public decimal PercentageValue { get; set; }

    }

    public class Investor
    {
        public string Account { get; set; }
        public string Name { get; set; }

        public string MNO { get; set; }

        public decimal InvestedAmount { get; set; }
    }
}