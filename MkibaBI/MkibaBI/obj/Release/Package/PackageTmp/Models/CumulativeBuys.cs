﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class CumulativeBuys
    {
        public string c { get; set; }
        public decimal Safaricom { get; set; }

        public decimal Airtel { get; set; }

        public decimal Unallocated { get; set; }

        public decimal Total { get; set; }
    }
}