﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MkibaBI.DAO;
using MkibaBI.DAO.security;

namespace MkibaBI.Controllers
{ //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
  // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1")]
    public class IssuancesController : Controller
    {
        private Mkiba db = new Mkiba();

        // GET: Issuances
        public async Task<ActionResult> Index()
        {
            return View(await db.Issuances.ToListAsync());
        }

        // GET: Issuances/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Issuance issuance = await db.Issuances.FindAsync(id);
            if (issuance == null)
            {
                return HttpNotFound();
            }
            return View(issuance);
        }

        // GET: Issuances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Issuances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,IssuanceName,ISIN,StartDate,EndDate,Target")] Issuance issuance)
        {
            if (ModelState.IsValid)
            {
                db.Issuances.Add(issuance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(issuance);
        }

        // GET: Issuances/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Issuance issuance = await db.Issuances.FindAsync(id);
            if (issuance == null)
            {
                return HttpNotFound();
            }
            return View(issuance);
        }

        // POST: Issuances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,IssuanceName,ISIN,StartDate,EndDate,Target")] Issuance issuance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issuance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(issuance);
        }

        // GET: Issuances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Issuance issuance = await db.Issuances.FindAsync(id);
            if (issuance == null)
            {
                return HttpNotFound();
            }
            return View(issuance);
        }

        // POST: Issuances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Issuance issuance = await db.Issuances.FindAsync(id);
            db.Issuances.Remove(issuance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
