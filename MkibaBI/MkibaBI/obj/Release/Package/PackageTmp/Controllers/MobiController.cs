﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MkibaBI.DAO;
using MkibaBI.DAO.security;
using WebMatrix.WebData;
using WebMatrix.Data;
using RestSharp;
using Newtonsoft.Json;
using MkibaBI.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace MkibaBI.Controllers
{
    public class MobiController : Controller
    {
        private Mkiba db2 = new Mkiba();
        public ActionResult Index()
        {

            int regcount = db2.Account_Creation.ToList().Where(a => a.Active == true).Count();

            int regcount2 = db2.Account_Creation.ToList().Where(a => a.Active == true && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

            int by = (from c in db2.Bond_Payment_Audit
                      select new { c.CDSC_Number }).Distinct().Count();

            var db = Database.Open("Mkiba2");

            var selectQueryString = "SELECT * from Bond_Payment_FailedBids";

            int fy = db.Query(selectQueryString).ToList().Count();

            var selectQueryString2 = "SELECT * from Account_Creation where TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_Audit) And TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_FailedBids)";

            int ny = db.Query(selectQueryString2).ToList().Count();
            double per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;

            ViewBag.RegCount = regcount.ToString("#,##0.00");
            ViewBag.By = by.ToString("#,##0.00");
            ViewBag.PC = Math.Round(per, 2).ToString();
            ViewBag.FY = fy.ToString("#,##0.00");
            ViewBag.NY = ny.ToString("#,##0.00");
            ViewBag.TY = regcount2.ToString("#,##0.00");

            return View();
        }

        public ActionResult Index2()
        {

            int potensafari = db2.Account_Creation.ToList().Where(a => a.MNO_ == "SAFARICOM" && a.Active == true).Count();

            int potenairtel = db2.Account_Creation.ToList().Where(a => a.MNO_ == "AIRTEL" && a.Active == true).Count();

            var db = Database.Open("Mkiba2");

            var selectQueryString = "SELECT * from Bond_Payment_FailedBids";
            int actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM'").ToList().Count();

            int actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL'").Count();

            ViewBag.PS = potensafari.ToString("#,##0.00");
            ViewBag.PA = potenairtel.ToString("#,##0.00");
            ViewBag.AS = actualsafari.ToString("#,##0.00");
            ViewBag.AA = actualairtel.ToString("#,##0.00");

            double c1 = (Convert.ToDouble(actualsafari) / Convert.ToDouble(potensafari)) * 100;
            double c2 = (Convert.ToDouble(actualairtel) / Convert.ToDouble(potenairtel)) * 100;

            ViewBag.CS = Math.Round(c1, 2).ToString();
            ViewBag.CA = Math.Round(c2, 2).ToString();
            int regcount = db2.Account_Creation.ToList().Where(a => a.Active == true).Count();
            string dt = DateTime.Now.ToString("dd/MM/yyyy");
            int regcount2 = db2.Account_Creation.ToList().Where(a => a.Active == true && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

            int by = (from c in db2.Bond_Payment_Audit
                      select new { c.CDSC_Number }).Distinct().Count();

            double per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;


            ViewBag.PC = Math.Round(per, 2).ToString();
            decimal? overall = db2.Bond_Payment_Audit.ToList().Sum(a => a.No_of_Notes_Applied);

            decimal? overallsafari = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "SAFARICOM").Sum(a => a.No_of_Notes_Applied);
            decimal? overallairtel = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "AIRTEL").Sum(a => a.No_of_Notes_Applied);

            decimal? overallt = db2.Bond_Payment_Audit.ToList().Where(a => a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);

            decimal? overallsafarit = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);
            decimal? overallairtelt = db2.Bond_Payment_Audit.ToList().Where(a => a.MNO_ == "AIRTEL" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Sum(a => a.No_of_Notes_Applied);

            ViewBag.OO = Convert.ToDouble(overall).ToString("#,##0.00");
            ViewBag.OS = Convert.ToDouble(overallsafari).ToString("#,##0.00");
            ViewBag.OA = Convert.ToDouble(overallairtel).ToString("#,##0.00");

            ViewBag.OOT = Convert.ToDouble(overallt).ToString("#,##0.00");
            ViewBag.OST = Convert.ToDouble(overallsafarit).ToString("#,##0.00");
            ViewBag.OAT = Convert.ToDouble(overallairtelt).ToString("#,##0.00");

            return View();
        }

        public ActionResult Index3()
        {
            int failedreg = db2.AccountC_FailedReg.ToList().Count();
            var db = Database.Open("Mkiba2");
            var fail = db.Query("select IsNull(COUNT(ReceiptNumber),'0') as 'CC' from Bond_Payment_FailedBids").ToList();
            var sf = db.Query("select IsNull(SUM(CAST(AmountPaid As money)),'0') As 'Total' from Bond_Payment_FailedBids").ToList();
            string vf = "";
            string ff = "";
            foreach (var d in sf)
            {
                vf = d.Total.ToString();
            }
            foreach (var q in fail)
            {
                ff = q.CC.ToString();
            }
            string comfR = "", Rerr = "";
            var commonreg = db.Query("select top 1 Reason,IsNull(Count(ID_),'0') as 'RError' from AccountC_FailedReg group by Reason order by Count(ID_) desc").ToList();

            foreach (var d in commonreg)
            {
                comfR = d.Reason.ToString();
                Rerr = d.RError.ToString();
            }

            string comfRB = "", RerrB = "";
            var commonbid = db.Query("select top 1 Reason,IsNull(Count(ReceiptNumber),'0') as 'RError' from Bond_Payment_FailedBids group by Reason order by Count(ReceiptNumber) desc").ToList();
            foreach (var d in commonbid)
            {
                comfRB = d.Reason.ToString();
                RerrB = d.RError.ToString();
            }
            decimal? avgR = 0;
            try
            {
                avgR = db2.AccountC_FailedReg.ToList().Average(a => a.AmountPaid);

            }
            catch (Exception)
            {

                avgR = 0;
            }
            try
            {
                ViewBag.FailedReg = failedreg.ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.FailedReg = "0";
            }
            try
            {
                ViewBag.FB = Math.Round(Convert.ToDecimal(ff), 2).ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.FB = "0";
            }
            ViewBag.CommonError = comfR.ToString();
            try
            {
                ViewBag.CommonOccur = Convert.ToDouble(Rerr).ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.CommonOccur = "0";
            }
            ViewBag.CommonErrorB = comfRB.ToString();
            try
            {
                ViewBag.CommonOccurB = Convert.ToDouble(RerrB).ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.CommonOccurB = "0";
            }
            try
            {
                ViewBag.SF = Math.Round(Convert.ToDecimal(vf), 2).ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.SF = "0";
            }
            try
            {
                ViewBag.AVR = Math.Round(Convert.ToDecimal(avgR), 2).ToString("#,##0.00");

            }
            catch (Exception)
            {

                ViewBag.AVR = "0";
            }
            try
            {
                var sfv = db.Query("select IsNull(AVG(CAST(AmountPaid As money)),'0') As 'Total' from Bond_Payment_FailedBids").ToList();
                string vfv = "";
                foreach (var d in sfv)
                {
                    vfv = d.Total.ToString();
                }
                ViewBag.FAV = Math.Round(Convert.ToDecimal(vfv), 2).ToString();

            }
            catch (Exception)
            {

                ViewBag.FAV = "0";

            }
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');

            //var sfc = db.Query("select IsNull(Reason,'None') As 'Reason',Date_Created,IsNull(AmountPaid,'0') As 'AmountPaid' from Bond_Payment_FailedBids").ToList();
            //var client = new RestClient(baseUrl);
            //var request = new RestRequest("failedbid", Method.GET);
            //IRestResponse response = client.Execute(request);
            // string validate = response.Content;
            List<FailedBids> dataList = falls();



            try
            {
                ViewBag.SFP = dataList.ToList();
            }
            catch (Exception)
            {

                ViewBag.SFP = null;
            }
            return View();
        }

        public List<FailedBids> falls()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids order by Date_Created desc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<FailedBids>();
            while (reader.Read())
            {
                var accountDetails = new FailedBids
                {

                    Reason = reader.GetValue(0).ToString(),
                    AmountPaid = reader.GetValue(1).ToString(),
                    Date_Created = Convert.ToDateTime(reader.GetValue(2)).ToString("dd/MM/yyyy:HH:mm:ss")
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public ActionResult Index4()
        {

            return View();
        }

        public ActionResult Index5()
        {

            return View();
        }

    }
}