﻿using System;

using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Web.Security;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Migrations;
using MkibaBI.DAO;
using MkibaBI.DAO.security;
using MkibaBI.Models;

namespace MkibaBI.Controllers
{
    public class AccountController : Controller
    {
        private SBoardContext db = new SBoardContext();

        static int counter = 0;
        static object lockObj = new object();
        SBoardContext Context = new SBoardContext();
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {

            bool actv = true;
            if (ModelState.IsValid)
            {
                model.Password = ComputeHash(model.Password, new SHA256CryptoServiceProvider());
                var user = Context.Users.Where(u => u.Username == model.Username && u.Password == model.Password && u.LockCount==0 && u.IsActive==true).FirstOrDefault();
                if (user != null)
                {
                    var roles=user.Roles.Select(m => m.RoleName).ToArray();
                  
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.UserId;
                    serializeModel.FirstName = user.FirstName;
                    serializeModel.LastName = user.LastName;
                    serializeModel.roles = roles;
                    string role = user.role;
                    actv = user.IsActive;
                   string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.Email,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(15),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);



                    if(roles.Contains(role))
                    {
                        counter = 0;
                       return Redirect("~/"+role+"/Company");
                    }
                 else
                    {
                        return RedirectToAction("Index", "Account");
                    }
                }
                if (counter == 0)
                {
                    var users = db.Users.ToList().Where(a => a.Username == model.Username);
                    string name = "";
                    foreach (var p in users)
                    {
                        name = p.Username;
                    }
                    if (name == model.Username)
                    {
                        lock (lockObj)
                        {
                            counter++;
                        }
                        System.Web.HttpContext.Current.Session["Status"] = model.Username;
                    }

                }
                else
                {
                    var users = db.Users.ToList().Where(a => a.Username == model.Username);
                    string name = "";
                    foreach (var p in users)
                    {
                        name = p.Username;
                    }
                    if (name == model.Username)
                    {
                        if(name == System.Web.HttpContext.Current.Session["Status"].ToString())
                        {
                        lock (lockObj)
                        {
                            counter++;
                        }
                        }
                        else
                        {
                            counter = 1;
                            System.Web.HttpContext.Current.Session["Status"] = model.Username;
                        }
                      

                    }
                }
                  
             

                var locks = db.Users.ToList().Where(a => a.Username == model.Username && a.LockCount!=0);
                string name2 = "";

                 foreach(var x in locks)
                {
                    name2 = x.Username;
                }
                if (name2 == null)
                {
                    name2 = "@";
                }

                if (name2 == model.Username)
                {
                    ViewBag.Attempts = "Contact the administrator you have been locked out";
                }
                else
                {
                    if (counter > 3)
                    {
                        var users = db.Users.ToList().Where(b=> b.Username == model.Username);
                        int id = 0;
                        foreach (var p in users)
                        {
                           id= p.UserId;
                        }
                        //lock the user
                       
                        var a = db.Users.Find(id);
                        a.LockCount =5;
                        db.Users.AddOrUpdate(a);
                        db.SaveChanges();
                        ViewBag.Attempts = "Contact the administrator you have been locked out";
                    }
                    else
                    {
                        ViewBag.Attempts = "Number of Login attempts " + counter;
                    }

                }
                if(actv == false){
                    ModelState.AddModelError("", "This user is deactivated. Please contact your administrator'");
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username and/or password");
          
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Account", null);
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

     
    }
}