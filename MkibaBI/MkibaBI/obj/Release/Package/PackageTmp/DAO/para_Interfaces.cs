//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class para_Interfaces
    {
        public string Category { get; set; }
        public string RouteID { get; set; }
        public string LocalListenerIP { get; set; }
        public string LocalListenerPort { get; set; }
        public string LocalSenderIP { get; set; }
        public string LocalSenderPort { get; set; }
        public string RemoteListenerIP { get; set; }
        public string RemoteListenerPort { get; set; }
        public string RemoteSenderIP { get; set; }
        public string RemoteSenderPort { get; set; }
        public int ID { get; set; }
    }
}
