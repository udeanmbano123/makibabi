﻿
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


using TrackerEnabledDbContext.Identity;

using MkibaBI.Models;

namespace MkibaBI.DAO
{
    public class  SBoardContext : TrackerIdentityContext<ApplicationUser>
    {
        public SBoardContext()
            : base("OBoardConnection")
        {}
        //public DbSet<Modules> Moduless { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }







        //public System.Data.Entity.DbSet<MkibaBI.Models.ApplicationUser> ApplicationUsers { get; set; }

    }



}