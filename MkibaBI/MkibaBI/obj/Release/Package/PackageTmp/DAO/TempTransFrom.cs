//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class TempTransFrom
    {
        public string company { get; set; }
        public string cds_number { get; set; }
        public string shares { get; set; }
        public decimal batch_ref { get; set; }
        public System.DateTime date_of_capture { get; set; }
        public string capturedBy { get; set; }
        public string status { get; set; }
        public Nullable<bool> DebtSecurity { get; set; }
        public int ID { get; set; }
    }
}
