//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trade
    {
        public Nullable<decimal> Transid { get; set; }
        public Nullable<decimal> TradingAmount { get; set; }
        public string CR { get; set; }
        public string DR { get; set; }
        public Nullable<System.DateTime> tradedate { get; set; }
        public string FundingStatus { get; set; }
        public int ID { get; set; }
    }
}
