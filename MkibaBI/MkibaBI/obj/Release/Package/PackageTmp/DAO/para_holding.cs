//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class para_holding
    {
        public int ID_ { get; set; }
        public string Category { get; set; }
        public string Issuer_Code { get; set; }
        public string Debt_Type { get; set; }
        public string Security_Description { get; set; }
        public Nullable<System.DateTime> Date_Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<decimal> GlobalLimit { get; set; }
        public Nullable<decimal> IndividualLimit { get; set; }
        public Nullable<decimal> FirstLimit { get; set; }
        public Nullable<decimal> DailyLimit { get; set; }
        public string emailFrom { get; set; }
        public string SMTPClient { get; set; }
        public string NetworkCredUsername { get; set; }
        public string NetworkCredPassword { get; set; }
        public string SMTPport { get; set; }
        public Nullable<decimal> BidRatio { get; set; }
        public string AirtelBidUsername { get; set; }
        public string AirtelBidPassword { get; set; }
        public string AirTelMarchantBillerCode { get; set; }
        public Nullable<int> IPOSTATUS { get; set; }
        public Nullable<decimal> globLowerlimit { get; set; }
        public Nullable<decimal> InterestRate { get; set; }
        public Nullable<System.DateTime> IPOClosedDate { get; set; }
        public Nullable<decimal> Transaction_Limit { get; set; }
        public string NewIssuerCode { get; set; }
        public Nullable<System.DateTime> Listing_Date { get; set; }
        public string Issuer { get; set; }
        public Nullable<bool> Locked { get; set; }
        public Nullable<decimal> Multiples { get; set; }
    }
}
