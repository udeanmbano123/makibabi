//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class share_transfer
    {
        public int id { get; set; }
        public Nullable<decimal> amount_to_transfer { get; set; }
        public string transferor { get; set; }
        public string transferee { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string status { get; set; }
        public string query { get; set; }
        public string company { get; set; }
    }
}
