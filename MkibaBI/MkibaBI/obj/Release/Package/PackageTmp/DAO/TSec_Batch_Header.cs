//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class TSec_Batch_Header
    {
        public string Company { get; set; }
        public decimal Batch_Ref { get; set; }
        public string batch_type { get; set; }
        public System.DateTime Batch_Date { get; set; }
        public decimal Batch_Total { get; set; }
        public string Batch_Broker { get; set; }
        public string Batch_Forwarded_By { get; set; }
        public System.DateTime Batch_Forwarded_On { get; set; }
        public string Accepted_By { get; set; }
        public System.DateTime Accepted_On { get; set; }
        public string Status { get; set; }
        public Nullable<bool> DebtSecurity { get; set; }
        public string rejection { get; set; }
        public int ID { get; set; }
    }
}
