//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class OTC_Data_Import
    {
        public int id { get; set; }
        public string Company { get; set; }
        public string BuyOrderNum { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string SellOrderNum { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<System.DateTime> TradeDate { get; set; }
        public Nullable<int> SettlementStatus { get; set; }
        public Nullable<decimal> importid { get; set; }
    }
}
