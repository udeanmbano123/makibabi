//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MkibaBI.DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblTradesUpload
    {
        public string TransID { get; set; }
        public string InitDealNo { get; set; }
        public string InitSuffixNo { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> DealPrice { get; set; }
        public string DealDateTime { get; set; }
        public string Broker_Init { get; set; }
        public string Broker_Target { get; set; }
        public string Company { get; set; }
        public string InitDealType { get; set; }
        public string TargetDealType { get; set; }
        public string TargetDealNo { get; set; }
        public string TargetSuffixNo { get; set; }
        public Nullable<decimal> UpdateFlag { get; set; }
        public Nullable<decimal> UploadID { get; set; }
        public int ID { get; set; }
    }
}
