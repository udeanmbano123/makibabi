﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MkibaBI.Models
{
    public class WeekonWeekR
    {
        public string WeekEnding { get; set; }
        public string DisbCount { get; set; }
        public string DisbAmt { get; set; }
        public string RepayCount { get; set; }
        public string RepayAmt { get; set; }
        public string color { get; set; }
    }
}