﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
namespace MkibaBI.Models
{
    [TrackChanges]
  
    public class User
    {   [Key]
        public int UserId { get; set; }

        [Required]
        public String Username { get; set; }

        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        [Index(IsUnique = true)]
        public String Email { get; set; }

        [Required]
        [MinLength(6)]
        [RegularExpression(@"(?=.*\d)(?=.*[A-Za-z]).{6,}", ErrorMessage = "Your password must be at least 6 characters long and contain at least 1 letter and 1 number")]
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [Display(Name = "Confirm")]
        [Required]
        [CompareObsolete("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DataType(DataType.Password)]
        public String ConfirmPassword { get; set; }
        
        public String FirstName { get; set; }
       
        public String LastName { get; set; }
       
       
        public Boolean IsActive { get; set; }
        [DefaultValue(0)]
        public int LockCount { get; set; }
        public DateTime? CreateDate { get; set; }
        [Required]
        public string role { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}