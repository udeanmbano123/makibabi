namespace MkibaBI.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<MkibaBI.DAO.SBoardContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MkibaBI.DAO.SBoardContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //Role role = new Role { RoleName = "Admin" };
            //User user = new User { Username = "admin", Email = "admin@ymail.com", FirstName = "Admin", LastName = "a", Password = ComputeHash("123456", new SHA256CryptoServiceProvider()), ConfirmPassword = ComputeHash("123456", new SHA256CryptoServiceProvider()), IsActive = true, LockCount = 0, role = role.RoleName, CreateDate = DateTime.UtcNow, Roles = new List<Role>() };
            //user.Roles.Add(role);

            //context.Users.Add(user);
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
