﻿using MkibaBI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using WebMatrix.Data;

namespace MkibaBI.Controllers
{
  
    public class GraphController : ApiController
    {
       

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("registered/{s}")]
        [AllowAnonymous]
        public List<Registered> GetEarners(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select convert(date, Account_Creation.Date_Created) as 'Date',Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [Account_Creation] inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='"+s+ "'  and CAST(Account_Creation.Date_Created as date)>=cast('25-FEB-2019' as date) group by convert(date,Account_Creation. Date_Created) order by convert(date,Account_Creation.Date_Created) asc";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select convert(date, Account_Creation.Date_Created) as 'Date',Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [Account_Creation] inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' and CAST(Account_Creation.Date_Created as date)<cast('25-FEB-2019' as date) group by convert(date,Account_Creation. Date_Created) order by convert(date,Account_Creation.Date_Created) asc";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select convert(date, Account_Creation.Date_Created) as 'Date',Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [Account_Creation] inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' and CAST(Account_Creation.Date_Created as date)>=cast('25-May-2019' as date) and CAST(Account_Creation.Date_Created as date)<=cast('08-June-2019' as date) group by convert(date,Account_Creation. Date_Created) order by convert(date,Account_Creation.Date_Created) asc";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select convert(date, Account_Creation.Date_Created) as 'Date',Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [Account_Creation] inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' group by convert(date,Account_Creation.Date_Created) order by convert(date,Account_Creation.Date_Created) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Registered>();
            while (reader.Read())
            {
                var accountDetails = new Registered
                {

                    Date = Convert.ToDateTime(reader.GetValue(0)).ToString("dd-MM-yyyy"),
                   Total =reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("agerange/{s}")]
        [AllowAnonymous]
        public List<AgeRange> GetAge(string s)
        {
            //return listEmp.First(e => e.ID == id); 
            string q1, q2, q3, q4, q5, q6, q7, q8, q9,q10,q11,q12;

            q1 = "select '0-18' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 0 and 18) and Company='" + s  +"'";
            q2 = "union all select '19-30' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age  between 19 and 30) and Company='" + s  +"'";
                q3 = "union all select '31-40' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 31 and 40) and Company='" + s  +"'";
                q4 = "union all select '41-50' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 41 and 50) and Company='" + s  +"'";
                q5 = "union all select '51-60' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 51 and 60) and Company='" + s  +"'";
                q6 = "union all select '61-70' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 61 and 70) and Company='" + s  +"'";
                q7 = "union all select '71-80' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysis where (Age between 71 and 80) and Company='" + s  +"'";
                q8 = "union all select '81-90' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysis where (Age between 81 and 90) and Company='" + s  +"'";
                q9 = "union all select '91-100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysis where (Age between 91 and 100) and Company='" + s  +"'";
                q10 = "union all select '>100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysis where Age > 100 and Company='" + s  +"'";
            q11 = "union all select '<0' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysis where Age < 0 and Company='" + s  +"'";
                q12 = "union all select 'No age' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysis where Age is null and Company='" + s  +"'";

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");

                q1 = "select '0-18' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 0 and 18) and Company='" + s  +"'";
                q2 = "union all select '19-30' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt','#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where (Age  between 19 and 30) and Company='" + s  +"'";
                q3 = "union all select '31-40' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 31 and 40) and Company='" + s  +"'";
                q4 = "union all select '41-50' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 41 and 50) and Company='" + s  +"'";
                q5 = "union all select '51-60' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 51 and 60) and Company='" + s  +"'";
                q6 = "union all select '61-70' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 61 and 70) and Company='" + s  +"'";
                q7 = "union all select '71-80' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisOld where (Age between 71 and 80) and Company='" + s  +"'";
                q8 = "union all select '81-90' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where (Age between 81 and 90) and Company='" + s  +"'";
                q9 = "union all select '91-100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where (Age between 91 and 100) and Company='" + s  +"'";
                q10 = "union all select '>100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where Age > 100 and Company='" + s  +"'";
                q11 = "union all select '<0' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where Age < 0 and Company='" + s  +"'";
                q12 = "union all select 'No age' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisOld where Age is null and Company='" + s  +"'";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");

                q1 = "select '0-18' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 0 and 18) and Company='" + s + "'";
                q2 = "union all select '19-30' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt','#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where (Age  between 19 and 30) and Company='" + s + "'";
                q3 = "union all select '31-40' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 31 and 40) and Company='" + s + "'";
                q4 = "union all select '41-50' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 41 and 50) and Company='" + s + "'";
                q5 = "union all select '51-60' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 51 and 60) and Company='" + s + "'";
                q6 = "union all select '61-70' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 61 and 70) and Company='" + s + "'";
                q7 = "union all select '71-80' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from AgeAnalysisNew where (Age between 71 and 80) and Company='" + s + "'";
                q8 = "union all select '81-90' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where (Age between 81 and 90) and Company='" + s + "'";
                q9 = "union all select '91-100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where (Age between 91 and 100) and Company='" + s + "'";
                q10 = "union all select '>100' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where Age > 100 and Company='" + s + "'";
                q11 = "union all select '<0' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where Age < 0 and Company='" + s + "'";
                q12 = "union all select 'No age' as 'Range',isNull(count(TelephoneNumber), 0) as 'Total', isNull(sum(No_of_Notes_Applied), 0) as 'Amt' ,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AgeAnalysisNew where Age is null and Company='" + s + "'";

            }
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = q1+q2+q3+q4+q5+q6+q7+q8+q9+q10+q11+q12;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<AgeRange>();
            while (reader.Read())
            {
                var accountDetails = new AgeRange
                {

                    Range = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("piechart/{s}")]
        [AllowAnonymous]
        public List<SFPie> GetPie(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            q1 = "select distinct count(Account_Creation.identification) As Total,'Sucessful Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "'";
            //if (s == "KE5000006766")
            //{
            //    q2 = "select distinct Count(identification) As Total,'Failed Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg CAST(Date_Created as date)<CAST('13-APR-2018' AS date)";

            //}

            //if (s == "KE5000007210")
            //{
            //    q2 = "select distinct Count(identification) As Total,'Failed Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg where CAST(Date_Created as date)>CAST('13-APR-2018' AS date) and CAST(Date_Created as date)<=CAST('11-SEPT-2018' AS date)";

            //}
            q2 = "select distinct Count(identification) As Total,'Failed Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg where CAST(Date_Created as date)>CAST('25-FEB-2019' AS date)";

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
           q2 = "select distinct Count(identification) As Total,'Failed Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg where CAST(Date_Created as date)<CAST('25-FEB-2019' AS date)";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                q2 = "select distinct Count(identification) As Total,'Failed Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg where CAST(Date_Created as date)>CAST('25-May-2019' AS date)";

            }
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct count(identification) As Total,'Successful Registrations' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation union all select distinct Count(identification) As Total,'Failed Registrations' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg union all   select  Count(CDSC_Number) As 'Total','Closed Account' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Active=0";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<SFPie>();
            while (reader.Read())
            {
                var accountDetails = new SFPie
                {

                    Total = reader.GetValue(0).ToString(),
                    Reg = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("mno/{s}")]
        [AllowAnonymous]
        public List<MNOs> GetMn(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
           SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='"+s+ "' and Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Bond_Payment_Audit.Date_Created as date)>=cast('25-FEB-2019' as date) group by Account_Creation.MNO_";

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' and Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Bond_Payment_Audit.Date_Created as date)<cast('25-FEB-2019' as date) group by Account_Creation.MNO_";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' and Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Bond_Payment_Audit.Date_Created as date)>cast('25-May-2019' as date) group by Account_Creation.MNO_";

            }

            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "' and Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') group by Account_Creation.MNO_";

            }

            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<MNOs>();
            while (reader.Read())
            {
                var accountDetails = new MNOs
                {

                    MNO = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("mnoR/{s}")]
        [AllowAnonymous]
        public List<MNOs> GetMnR(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation  where  Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Account_Creation.Date_Created as date)>=cast('25-FEB-2019' as date) group by Account_Creation.MNO_";

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Account_Creation.Date_Created as date)<cast('25-FEB-2019' as date) group by Account_Creation.MNO_";

            }

            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Account_Creation.Date_Created as date)>cast('25-May-2019' as date) group by Account_Creation.MNO_";

            }

            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') group by Account_Creation.MNO_";

            }

            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<MNOs>();
            while (reader.Read())
            {
                var accountDetails = new MNOs
                {

                    MNO = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("registered")]
        [AllowAnonymous]
        public List<Registered> GetEarnerss()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select convert(date, Account_Creation.Date_Created) as 'Date',Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [Account_Creation] inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number and CAST(Account_Creation.Date_Created as date)>=cast('25-FEB-2019' as date)  group by convert(date,Account_Creation. Date_Created) order by convert(date,Account_Creation.Date_Created) asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Registered>();
            while (reader.Read())
            {
                var accountDetails = new Registered
                {

                    Date = Convert.ToDateTime(reader.GetValue(0)).ToString("dd-MM-yyyy"),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("agerange")]
        [AllowAnonymous]
        public List<AgeRange> GetAgea()
        {
            //return listEmp.First(e => e.ID == id); 
            string q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12;

            q1 = "select '0-18' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 0 and 18) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q2 = "union all select '19-30' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 19 and 30) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q3 = " union all select '31-40' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where  (Age between 31 and 40) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q4 = "  union all select '41-50' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 41 and 50) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q5 = " union all select '51-60' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 51 and 60) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q6 = " union all select '61-70' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 61 and 70) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q7 = " union all select '71-80' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 71 and 80) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q8 = " union all select '81-90' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 81 and 90) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q9 = " union all select '91-100' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age between 91 and 100) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q10 = " union all select '>100' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age> 100) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q11 = " union all select '<0' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age<0) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q12 = " union all select 'No Age' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg inner join Bond_Payment_Audit ON VwAgeReg.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where (Age is null) and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = q1 + q2 + q3 + q4 + q5 + q6 + q7 + q8 + q9+q10+q11+q12;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<AgeRange>();
            while (reader.Read())
            {
                var accountDetails = new AgeRange
                {

                    Range = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("agerangeR")]
        [AllowAnonymous]
        public List<AgeRange> GetAgeaaaa()
        {
            //return listEmp.First(e => e.ID == id); 
            string q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12;

            q1 = "select '0-18' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg  where (Age between 0 and 18) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q2 = "union all select '19-30' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg  where (Age between 19 and 30) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q3 = " union all select '31-40' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where  (Age between 31 and 40) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q4 = "  union all select '41-50' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where (Age between 41 and 50) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q5 = " union all select '51-60' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg  where (Age between 51 and 60) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q6 = " union all select '61-70' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg  where (Age between 61 and 70) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q7 = " union all select '71-80' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where (Age between 71 and 80) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q8 = " union all select '81-90' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where (Age between 81 and 90) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q9 = " union all select '91-100' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where (Age between 91 and 100) and CAST(Dat as date)>=cast('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q10 = "union all select '>100' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where Age > 100 and CAST(Dat as date)>= cast('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q11 = "union all select '<0' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where Age < 0 and CAST(Dat as date)>= cast('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q12 = "union all select 'Undefined' as 'Range',(Select Count(VwAgeReg.CDSC_Number)from VwAgeReg where Age is null and CAST(Dat as date)>= cast('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = q1 + q2 + q3 + q4 + q5 + q6 + q7 + q8 + q9 + q10 + q11 + q12;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<AgeRange>();
            while (reader.Read())
            {
                var accountDetails = new AgeRange
                {

                    Range = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("piechart")]
        [AllowAnonymous]
        public List<SFPie> GetPiea()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            //q1 = "select distinct count(Account_Creation.identification) As Total,'Sucessful Registration' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Bond_Payment_Audit.Company='" + s + "'";
            
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select distinct count(identification) As Total,'Successful Registrations' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation union all select distinct Count(identification) As Total,'Failed Registrations' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from AccountC_FailedReg union all   select  Count(CDSC_Number) As 'Total','Closed Account' As Reg,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Active=0";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<SFPie>();
            while (reader.Read())
            {
                var accountDetails = new SFPie
                {

                    Total = reader.GetValue(0).ToString(),
                    Reg = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("mno")]
        [AllowAnonymous]
        public List<MNOs> GetMns()
        {
           //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation inner join Bond_Payment_Audit ON Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Bond_Payment_Audit.Date_Created as date)>=cast('25-FEB-2019' as date) group by Account_Creation.MNO_ ";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<MNOs>();
            while (reader.Read())
            {
                var accountDetails = new MNOs
                {

                    MNO = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("mnoR")]
        [AllowAnonymous]
        public List<MNOs> GetMnsR()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Account_Creation.MNO_,Count(Account_Creation.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation where Account_Creation.MNO_ in('SAFARICOM','AIRTEL','PESALINK') and CAST(Account_Creation.Date_Created as date)>=cast('25-FEB-2019' as date) group by Account_Creation.MNO_ ";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<MNOs>();
            while (reader.Read())
            {
                var accountDetails = new MNOs
                {

                    MNO = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("daily/{s}")]
        [AllowAnonymous]
        public List<DailyBuy> Daily(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select convert(date, Date_Created) as 'Date', (Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='SAFARICOM' and Company='"+ s + "') as 'SAFARICOM',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='AIRTEL' and Company='" + s + "') as 'AIRTEL',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='PESALINK' and Company='" + s + "') as 'PESALINK' FROM [Bond_Payment_Audit] q where CAST(q.Date_Created as date)>=cast('25-FEB-2019' as date)  group by convert(date, q.Date_Created) order by convert(date,q.Date_Created) asc";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD","");
                sqlCmd.CommandText = "select convert(date, Date_Created) as 'Date', (Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='SAFARICOM' and Company='" + s + "') as 'SAFARICOM',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='AIRTEL' and Company='" + s + "') as 'AIRTEL',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='PESALINK' and Company='" + s + "') as 'PESALINK' FROM [Bond_Payment_Audit] q where CAST(q.Date_Created as date)<cast('25-FEB-2019' as date) group by convert(date, q.Date_Created) order by convert(date,q.Date_Created) asc";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select convert(date, Date_Created) as 'Date', (Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='SAFARICOM' and Company='" + s + "') as 'SAFARICOM',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='AIRTEL' and Company='" + s + "') as 'AIRTEL',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='PESALINK' and Company='" + s + "') as 'PESALINK' FROM [Bond_Payment_Audit] q where CAST(q.Date_Created as date)>cast('25-May-2019' as date) group by convert(date, q.Date_Created) order by convert(date,q.Date_Created) asc";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select convert(date, Date_Created) as 'Date', (Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='SAFARICOM' and Company='" + s + "') as 'SAFARICOM',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='AIRTEL' and Company='" + s + "') as 'AIRTEL',(Select Sum(No_of_Notes_Applied) from Bond_Payment_Audit p where convert(date, p.Date_Created)=convert(date, q.Date_Created) and p.MNO_='PESALINK' and Company='" + s + "') as 'PESALINK' FROM [Bond_Payment_Audit] q group by convert(date, q.Date_Created) order by convert(date,q.Date_Created) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyBuy>();
            while (reader.Read())
            {
                var accountDetails = new DailyBuy
                {

                     Date = Convert.ToDateTime(reader.GetValue(0)).ToString("yyyy-MM-dd"),
                    SAFARICOM = reader.GetValue(1).ToString(),
                    AIRTEL = reader.GetValue(2).ToString()
            
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("dailys/{s}")]
        [AllowAnonymous]
        public List<Daily> Dailys(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select MNO_,convert(date, Date_Created) as 'Date',Sum(No_of_Notes_Applied) As 'Total' FROM [Bond_Payment_Audit] where MNO_ in('SAFARICOM','AIRTEL','PESALINK') and Company='" + s + "' and CAST(Bond_Payment_Audit.Date_Created as date)>=cast('25-FEB-2019' as date) group by MNO_,convert(date, Date_Created) order by convert(date,Date_Created) asc";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select MNO_,convert(date, Date_Created) as 'Date',Sum(No_of_Notes_Applied) As 'Total' FROM [Bond_Payment_Audit] where MNO_ in('SAFARICOM','AIRTEL','PESALINK') and Company='" + s + "' and CAST(Bond_Payment_Audit.Date_Created as date)<cast('25-FEB-2019' as date) group by MNO_,convert(date, Date_Created) order by convert(date,Date_Created) asc";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select MNO_,convert(date, Date_Created) as 'Date',Sum(No_of_Notes_Applied) As 'Total' FROM [Bond_Payment_Audit] where MNO_ in('SAFARICOM','AIRTEL','PESALINK') and Company='" + s + "' and CAST(Bond_Payment_Audit.Date_Created as date)>cast('25-May-2019' as date) group by MNO_,convert(date, Date_Created) order by convert(date,Date_Created) asc";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select MNO_,convert(date, Date_Created) as 'Date',Sum(No_of_Notes_Applied) As 'Total' FROM [Bond_Payment_Audit] where MNO_ in('SAFARICOM','AIRTEL','PESALINK') and Company='" + s + "' group by MNO_,convert(date, Date_Created) order by convert(date,Date_Created) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Daily>();
            while (reader.Read())
            {
                var accountDetails = new Daily
                {

                    Date = Convert.ToDateTime(reader.GetValue(1)).ToString("dd/MM/yyyy"),
                     MNO_ = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(2).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("failedbid")]
        [AllowAnonymous]
        public List<FailedBids> falls()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids where CAST(Bond_Payment_FailedBids.Date_Created as date)>=cast('25-FEB-2019' as date) order by Date_Created desc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<FailedBids>();
            while (reader.Read())
            {
                var accountDetails = new FailedBids
                {

                   Reason = reader.GetValue(0).ToString(),
                    AmountPaid = reader.GetValue(1).ToString(),
                    Date_Created = Convert.ToDateTime(reader.GetValue(2)).ToString("dd/MM/yyyy:HH:mm:ss")
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("buyerrange/{s}")]
        [AllowAnonymous]
        public List<BuyerSpread> GetBuy(string s)
        {
            //return listEmp.First(e => e.ID == id); 
            string q1, q2, q3, q4, q5, q6, q7, q8, q9;

            q1 = "select '0-18' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 0 and 18 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q2 = "union all select '19-30' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 19 and 30 and Company='" + s + "'  and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q3 = "union all select '31-40' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 31 and 40 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q4 = "union all select '41-50' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 41 and 50 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q5 = "union all select '51-60' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 51 and 60 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q6 = "union all select '61-70' as 'Range',(Select sum(Holdings)from BuyerAge where Age between 61 and 70 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q7 = "union all select '71-80' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 71 and 80 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q8 = "union all select '81-90' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 81 and 90 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
            q9 = "union all select '91-100' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 91 and 100 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                q1 = "select '0-18' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 0 and 18 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q2 = "union all select '19-30' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 19 and 30 and Company='" + s + "'  and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q3 = "union all select '31-40' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 31 and 40 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q4 = "union all select '41-50' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 41 and 50 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q5 = "union all select '51-60' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 51 and 60 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q6 = "union all select '61-70' as 'Range',(Select sum(Holdings)from BuyerAge where Age between 61 and 70 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q7 = "union all select '71-80' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 71 and 80 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q8 = "union all select '81-90' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 81 and 90 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q9 = "union all select '91-100' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 91 and 100 and Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                q1 = "select '0-18' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 0 and 18 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q2 = "union all select '19-30' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 19 and 30 and Company='" + s + "'  and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q3 = "union all select '31-40' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 31 and 40 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q4 = "union all select '41-50' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 41 and 50 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q5 = "union all select '51-60' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 51 and 60 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q6 = "union all select '61-70' as 'Range',(Select sum(Holdings)from BuyerAge where Age between 61 and 70 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q7 = "union all select '71-80' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 71 and 80 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q8 = "union all select '81-90' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 81 and 90 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q9 = "union all select '91-100' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 91 and 100 and Company='" + s + "' and cast(Date_Created as date)>=CAST('25-MAY-2019' as date)) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                q1 = "select '0-18' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 0 and 18 and Company='" + s + "' ) as Total,'#' + CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q2 = "union all select '19-30' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 19 and 30 and Company='" + s + "'  ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q3 = "union all select '31-40' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 31 and 40 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q4 = "union all select '41-50' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 41 and 50 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q5 = "union all select '51-60' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 51 and 60 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q6 = "union all select '61-70' as 'Range',(Select sum(Holdings)from BuyerAge where Age between 61 and 70 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q7 = "union all select '71-80' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 71 and 80 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q8 = "union all select '81-90' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 81 and 90 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";
                q9 = "union all select '91-100' as 'Range',(Select sum(Holdings) from BuyerAge where Age between 91 and 100 and Company='" + s + "' ) as Total,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'";

            }
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText =q1+q2+q3+q4+q5+q6+q7+q8+q9;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<BuyerSpread>();
            while (reader.Read())
            {
                var accountDetails = new BuyerSpread
                {

                    Range = reader.GetValue(0).ToString(),
                    Total = reader.GetValue(1).ToString(),
                    color = reader.GetValue(2).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("bidders/{s}")]
        [AllowAnonymous]
        public List<Bidders> bidder(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='"+ s +"' and bpa.Date_Created>=CAST('25-FEB-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' and bpa.Date_Created<CAST('25-FEB-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' and bpa.Date_Created>CAST('25-May-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Bidders>();
            while (reader.Read())
            {
                var accountDetails = new Bidders
                {

                    CDSC_Number = reader.GetValue(0).ToString(),
                    ForeName = reader.GetValue(1).ToString(),
                    Surname= reader.GetValue(2).ToString(),
                    Holding= reader.GetValue(3).ToString(),
                    Fullnames= reader.GetValue(0).ToString()+" "+reader.GetValue(1).ToString() + " " + reader.GetValue(2).ToString(),
                    color= reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("biddersc/{s}")]
        [AllowAnonymous]
        public List<BiddersC> bidderc(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' and bpa.Date_Created>=CAST('25-FEB-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' and bpa.Date_Created<CAST('25-FEB-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' and bpa.Date_Created>CAST('25-MAY-2019' as date) group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "Select bpa.CDSC_Number,(select top 1 ac.Surname_CompanyName from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as Surname_CompanyName ,(select top 1 ac.OtherNames from Account_Creation ac where ac.TelephoneNumber=bpa.TelephoneNumber) as OtherNames, sum(bpa.AmountPaid) as Holding,'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit  bpa  where bpa.Company='" + s + "' group by bpa.CDSC_Number,bpa.TelephoneNumber order by sum(bpa.AmountPaid)";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<BiddersC>();
            while (reader.Read())
            {
                var accountDetails = new BiddersC
                {

                    CDSC_Number = reader.GetValue(0).ToString(),
                    ForeName = reader.GetValue(1).ToString(),
                    Surname = reader.GetValue(2).ToString(),
                    Activity = reader.GetValue(3).ToString(),
                    fullnames = reader.GetValue(0).ToString() + " " + reader.GetValue(1).ToString() + " " + reader.GetValue(2).ToString(),
                    color = reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("weekonb/{s}")]
        [AllowAnonymous]
        public List<WeekonWeekB> weekB(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;

            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD","");
  sqlCmd.CommandText = "Exec WeekonWeekBuysO @com='"+ s +"'";

            }
            else
            {
                sqlCmd.CommandText = "Exec WeekonWeekBuys @com='" + s + "'";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "Exec WeekonWeekBuysLO @com='" + s + "'";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<WeekonWeekB>();
            while (reader.Read())
            {
                var accountDetails = new WeekonWeekB
                {

                    WeekEnding = Convert.ToDateTime(reader.GetValue(0)).ToString("dd/MM/yyyy"),
                    DisbCount = reader.GetValue(1).ToString(),
                    DisbAmt= reader.GetValue(2).ToString(),
                    RepayCount = reader.GetValue(3).ToString(),
                    RepayAmt = reader.GetValue(4).ToString(),
                    color= reader.GetValue(5).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("regB/{s}")]
        [AllowAnonymous]
        public List<RR> regB(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select BrokerReference,c.Company_name,count(a.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where cast(a.Date_Created as date)>=CAST('25-FEB-2019' as date)  group by BrokerReference,c.Company_name";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,count(a.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where cast(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by BrokerReference,c.Company_name";


            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,count(a.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where cast(a.Date_Created as date)>=CAST('25-May-2019' as date)  group by BrokerReference,c.Company_name";


            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,count(a.CDSC_Number) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Account_Creation a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where cast(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by BrokerReference,c.Company_name";


            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<RR>();
            while (reader.Read())
            {
                var accountDetails = new RR
                {

                    BrokerReference = reader.GetValue(0).ToString(),
                    Company_name = reader.GetValue(1).ToString(),
                    Total = reader.GetValue(2).ToString(),
                   color = reader.GetValue(3).ToString()

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("buyB/{s}")]
        [AllowAnonymous]
        public List<BB> buyB(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select BrokerReference,c.Company_name,sum(a.No_of_Notes_Applied) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where a.Company='" + s + "' and cast(a.Date_Created as date)>=CAST('25-FEB-2019' as date)  group by BrokerReference,c.Company_name";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,sum(a.No_of_Notes_Applied) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where a.Company='" + s + "' and cast(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by BrokerReference,c.Company_name";

            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,sum(a.No_of_Notes_Applied) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where a.Company='" + s + "' and cast(a.Date_Created as date)>CAST('25-May-2019' as date)  group by BrokerReference,c.Company_name";

            }
            if (s=="KE5000006766")
            {
                s = s.Replace("OLD", "");
                sqlCmd.CommandText = "select BrokerReference,c.Company_name,sum(a.No_of_Notes_Applied) As 'Total','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit a inner join Client_Companies c ON a.BrokerReference=c.Company_Code where a.Company='" + s + "'  group by BrokerReference,c.Company_name";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<BB>();
            while (reader.Read())
            {
                var accountDetails = new BB
                {

                    BrokerReference = reader.GetValue(0).ToString(),
                    Company_name = reader.GetValue(1).ToString(),
                    Total = reader.GetValue(2).ToString(),
                    color = reader.GetValue(3).ToString()

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("concentrations/{s}")]
        [AllowAnonymous]
        public List<CC> conTT(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2,q3,q4,q5,q6,q7;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;
            q1 = "Select 'Total Registrations' AS 'C' ,sum(No_of_Notes_Applied) AS 'T','100' As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit where Company='"+ s +"'";
            q2 = "union all select  'Total Top 10 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / (Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "'))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 10 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            q3 = "union all select  'Total Top 20 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 20 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            q4 = "union all select  'Total Top 40 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 40 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            q5 = "union all select 'Total Top 60 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 60 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            q6 = "union all select 'Total Top 80 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 80 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            q7 = "union all select  'Total Top 100 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 100 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-FEB-2019' as date)) as innerTable";
            if (s.Contains("OLD"))
            {
                s = s.Replace("OLD", "");
                q1 = "Select 'Total Registrations' AS 'C' ,sum(No_of_Notes_Applied) AS 'T','100' As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit where Company='" + s + "'";
                q2 = "union all select  'Total Top 10 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / (Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "'))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 10 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";
                q3 = "union all select  'Total Top 20 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 20 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";
                q4 = "union all select  'Total Top 40 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 40 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";
                q5 = "union all select 'Total Top 60 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 60 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";
                q6 = "union all select 'Total Top 80 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 80 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";
                q7 = "union all select  'Total Top 100 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 100 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)<CAST('25-FEB-2019' as date)) as innerTable";


            }
            if (s.Contains("LATEST"))
            {
                s = s.Replace("LATEST", "");
                q1 = "Select 'Total Registrations' AS 'C' ,sum(No_of_Notes_Applied) AS 'T','100' As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit where Company='" + s + "'";
                q2 = "union all select  'Total Top 10 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / (Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "'))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 10 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";
                q3 = "union all select  'Total Top 20 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 20 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";
                q4 = "union all select  'Total Top 40 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 40 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";
                q5 = "union all select 'Total Top 60 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 60 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";
                q6 = "union all select 'Total Top 80 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 80 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";
                q7 = "union all select  'Total Top 100 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 100 * from Bond_Payment_Audit where Company='" + s + "' and cast(Date_Created as date)>=CAST('25-May-2019' as date)) as innerTable";


            }
            if (s== "KE5000006766")
            {
                s = s.Replace("OLD", "");
                q1 = "Select 'Total Registrations' AS 'C' ,sum(No_of_Notes_Applied) AS 'T','100' As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from Bond_Payment_Audit where Company='" + s + "'";
                q2 = "union all select  'Total Top 10 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / (Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "'))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 10 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";
                q3 = "union all select  'Total Top 20 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 20 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";
                q4 = "union all select  'Total Top 40 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 40 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";
                q5 = "union all select 'Total Top 60 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 60 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";
                q6 = "union all select 'Total Top 80 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' from (select top 80 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";
                q7 = "union all select  'Total Top 100 Registrations' AS 'C',sum(innerTable.No_of_Notes_Applied)AS 'T',(sum(innerTable.No_of_Notes_Applied) / ((Select sum(No_of_Notes_Applied)As 'Percentage' from Bond_Payment_Audit where Company='" + s + "')))*100 As 'Percentage','#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color'  from (select top 100 * from Bond_Payment_Audit where Company='" + s + "' ) as innerTable";


            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText ="select * from concentrations";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CC>();
            while (reader.Read())
            {
                var accountDetails = new CC
                {

                    C = reader.GetValue(0).ToString(),
                    T = reader.GetValue(1).ToString(),
                    Percentage = Math.Round(Convert.ToDecimal(reader.GetValue(2)),2).ToString(),
                    color = reader.GetValue(3).ToString()

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("weekonr")]
        [AllowAnonymous]
        public List<WeekonWeekR> weekR()
        {
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SELECT [WeekEnding],[DisbCount],[DisbAmt],[RepayCount],[RepayAmt],'#' +  CONVERT(VARCHAR(max), CRYPT_GEN_RANDOM(3), 2) as 'color' FROM [WeekonWeekB] order by [WeekEnding] desc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<WeekonWeekR>();
            while (reader.Read())
            {
                var accountDetails = new WeekonWeekR
                {

                    WeekEnding = Convert.ToDateTime(reader.GetValue(0)).ToString("dd/MM/yyyy"),
                    DisbCount = reader.GetValue(1).ToString(),
                    DisbAmt =reader.GetValue(2).ToString(),
                    RepayCount = reader.GetValue(3).ToString(),
                    RepayAmt = reader.GetValue(4).ToString(),
                    color = reader.GetValue(5).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

    }
}