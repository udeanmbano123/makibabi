﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MkibaBI.DAO;
using MkibaBI.DAO.security;
using WebMatrix.WebData;
using WebMatrix.Data;
using RestSharp;
using Newtonsoft.Json;
using MkibaBI.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using PagedList;
using System.Globalization;

namespace MkibaBI.Controllers
{// GET: User
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1")]
    public class UserController : Controller
    {
        private Mkiba db2 = new Mkiba();



        public ActionResult Company()
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');

            var dbsel = from f in db2.para_holding
                        select f;


            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "--Select--", Value = "--Select--" });
            int counter = 0;
            foreach (var row in dbsel)
            {
                //Adding every record to list  
                if (counter >= 1)
                {
                    selectlist.Add(new SelectListItem { Text = row.NewIssuerCode.ToString(), Value = row.NewIssuerCode.ToString() });
                    selectlist.Add(new SelectListItem { Text = row.NewIssuerCode.ToString() + "OLD", Value = row.NewIssuerCode.ToString() + "OLD" });
                    selectlist.Add(new SelectListItem { Text = row.NewIssuerCode.ToString() + "LATEST", Value = row.NewIssuerCode.ToString() + "LATEST" });
                }
                else
                {
                    selectlist.Add(new SelectListItem { Text = row.NewIssuerCode.ToString(), Value = row.NewIssuerCode.ToString() });
                
                }
                counter++;

            }
            ViewBag.Role = selectlist;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Company([Bind(Include = "Company")] Company registerreport)
        {

            if (ModelState.IsValid && registerreport.company != "--Select--")
            {
                System.Web.HttpContext.Current.Session["safe"] = registerreport.company;
                var p = db2.para_holding.ToList().Where(a => a.NewIssuerCode == registerreport.company.Replace("OLD", ""));
                foreach (var k in p)
                {
                    if (registerreport.company.Contains("OLD"))
                    {
                        System.Web.HttpContext.Current.Session["safe2"] = k.Issuer + " . " + k.NewIssuerCode + "OLD";

                        System.Web.HttpContext.Current.Session["safe"] = k.NewIssuerCode + "OLD";
                    }
                    else if (registerreport.company.Contains("LATEST"))
                    {
                        System.Web.HttpContext.Current.Session["safe2"] = k.Issuer + " . " + k.NewIssuerCode + "LATEST";

                        System.Web.HttpContext.Current.Session["safe"] = k.NewIssuerCode + "LATEST";
                    }
                    else
                    {
                        System.Web.HttpContext.Current.Session["safe2"] = k.Issuer + " . " + k.NewIssuerCode;

                        System.Web.HttpContext.Current.Session["safe"] = k.NewIssuerCode;

                    }
                }
                return Redirect("~/User/Index");
            }
            return View();
        }
        public ActionResult Index()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            DateTime value = new DateTime(2019, 2, 25);
          
            int regcount = 0;
            int regcount2 = 0;
            int by = 0;
            int fy = 0;
            double per = 0.0;
            int regcountOverall = 0;
            decimal buyOverall = 0;
            int ny = 0;
            var ps = System.Web.HttpContext.Current.Session["safe"].ToString();
            try
            {
                regcount = (from v in db2.Account_Creation
                            where (v.Date_Created>=value)
                            select v).Count();
                if (ps.Contains("OLD"))
                {
                    regcount = (from v in db2.Account_Creation
                                where v.Date_Created<value
                                select v).Count();
                }else if (ps.Contains("LATEST"))
                {
                  
                        regcount = (from v in db2.Account_Creation
                                    where v.Date_Created>=value && v.Date_Created <= value
                                    select v).Count();
                    
                }
             

            }
            catch (Exception)
            {

                regcount = 0;
            }
            try
            {
                regcount2 = (from v in db2.Account_Creation
                            where  v.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")
                             select v).Count();

            }
            catch (Exception)
            {

                regcount2 = 0;
            }
            try
            {
                regcountOverall = db2.Account_Creation.Count();
            }
            catch (Exception f)
            {

            }

            try
            {
                buyOverall = (from c in db2.Bond_Payment_Audit
                              select new { c.CDSC_Number }).Distinct().Count();
            }
            catch (Exception f)
            {

            }

            try
            {
                string nme = ViewBag.Comp.ToString();
                by = (from c in db2.Bond_Payment_Audit
                      where c.Company == ps.Replace("OLD", "") && c.Date_Created >= value
                      select new { c.CDSC_Number }).Distinct().Count();

                if (ps.Contains("OLD"))
                {
                    by = (from c in db2.Bond_Payment_Audit
                          where c.Company == ps.Replace("OLD", "") && c.Date_Created < value
                          select new { c.CDSC_Number }).Distinct().Count();
                }

                if (ps== "KE5000006766")
                {
                    by = (from c in db2.Bond_Payment_Audit
                          where c.Company == ps.Replace("OLD", "")
                          select new { c.CDSC_Number }).Distinct().Count();
                }
            }
            catch (Exception)
            {

                by = 0;
            }
            var db = Database.Open("Mkiba2");

            
            var selectQueryString = db2.Bond_Payment_FailedBids.ToList().Where(a=>a.Date_Created>=value);

            if (ps.Contains("OLD"))
            {
                selectQueryString = db2.Bond_Payment_FailedBids.ToList().Where(a => a.Date_Created<value);

            }

            //if (ps == "KE5000007210")
            //{
            //    selectQueryString = "SELECT * from Bond_Payment_FailedBids where CAST(Date_Created as date)>CAST('13-APR-2018' AS date) and  CAST(Date_Created as date)<=CAST('11-SEPT-2018' AS date)";

            //}

            try
            {
                fy = selectQueryString.Count();

            }
            catch (Exception)
            {

                fy = 0;
            }
            var selectQueryString2 = "SELECT * from Account_Creation inner join Bond_Payment_Audit on Account_Creation.CDSC_Number=Bond_Payment_Audit.CDSC_Number  where Account_Creation.TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_Audit) And TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_FailedBids) and Bond_Payment_Audit.Company='"+ps+"'";

            try
            {
                ny = db.Query(selectQueryString2).Count();
            }
            catch (Exception)
            {

                ny = 0;
            }
            try
            {
                per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;

            }
            catch (Exception)
            {

                per = 0.0;
            }
            ViewBag.RegCount = regcount.ToString("#,##");
            ViewBag.By = by.ToString("#,##");
            ViewBag.PC = Math.Round(per, 2).ToString();
            ViewBag.FY = fy.ToString("#,##");
            ViewBag.NY = ny.ToString("#,##");
            ViewBag.TY = regcount2.ToString("#,##");

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            ViewBag.OR = regcountOverall.ToString("#,##");
            ViewBag.OB = buyOverall.ToString("#,##");

            ViewBag.Sec = ps;
            return View();
        }
        public ActionResult IndexP()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            DateTime value = new DateTime(2019, 2,25);
            int regcount = 0;
            int regcount2 = 0;
            int regcountOverall = 0;
            decimal buyOverall = 0;
            int by = 0;
            int fy = 0;
            double per = 0.0;
            int ny = 0;
            try
            {
 regcountOverall = db2.Account_Creation.Count();
           }
            catch (Exception f)
            {

            }
         
            
            try
               
            {
                regcount = db2.Account_Creation.Where(a =>a.Date_Created >= value).Count();

                if (nme.Contains("OLD"))
                {
                    regcount = db2.Account_Creation.Where(a =>a.Date_Created < value).Count();

                }
            }
            catch (Exception)
            {

                regcount = 0;
            }
            try
            {
                regcount2 = db2.Account_Creation.Where(a =>a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

            }
            catch (Exception)
            {

                regcount2 = 0;
            }

            try
            {
 buyOverall= (from c in db2.Bond_Payment_Audit
                     select new { c.CDSC_Number }).Distinct().Count();
            }
            catch(Exception f){

            }
           

            try
            {
                by = (from c in db2.Bond_Payment_Audit
                      where (c.Date_Created >= value) && c.Company==nme
                      select new { c.CDSC_Number }).Distinct().Count();

                if (nme.Contains("OLD"))
                {
                    by = (from c in db2.Bond_Payment_Audit
                          where (c.Date_Created < value) && c.Company==nme.Replace("OLD","") 
                          select new { c.CDSC_Number }).Distinct().Count();
                }
                if (nme== "KE5000006766")
                {
                    by = (from c in db2.Bond_Payment_Audit
                          where c.Company == nme.Replace("OLD", "")
                          select new { c.CDSC_Number }).Distinct().Count();
                }
            }
            catch (Exception)
            {

                by = 0;
            }
            var db = Database.Open("Mkiba2");

            var selectQueryString = "SELECT * from Bond_Payment_FailedBids";

            try
            {
                fy = db2.Bond_Payment_FailedBids.Where(a=>a.Date_Created>=value).Count();
                if (nme.Contains("OLD"))
                {
                    fy = db2.Bond_Payment_FailedBids.Where(a => a.Date_Created <value).Count();

                }

            }
            catch (Exception)
            {

                fy = 0;
            }
            var selectQueryString2 = "SELECT * from Account_Creation where TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_Audit) And TelephoneNumber not in (Select TelephoneNumber from Bond_Payment_FailedBids)";

            try
            {
                ny = db.Query(selectQueryString2).Count();
            }
            catch (Exception)
            {

                ny = 0;
            }
            try
            {
                per = ((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;

            }
            catch (Exception)
            {

                per = 0.0;
            }
            ViewBag.RegCount = regcount.ToString("#,##");
            ViewBag.By = by.ToString("#,##");
            ViewBag.PC = Math.Round(per, 2).ToString();
            ViewBag.FY = fy.ToString("#,##");
            ViewBag.NY = ny.ToString("#,##");
            ViewBag.TY = regcount2.ToString("#,##");
            ViewBag.OR=regcountOverall.ToString("#,##");
            ViewBag.OB=buyOverall.ToString("#,##");
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }


            return View();
        }

        public ActionResult Index2()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            try
            {


                DateTime value = new DateTime(2019, 2, 25);
                DateTime value2 = new DateTime(2019, 5, 25);
                DateTime value3 = new DateTime(2019, 6, 8);
                try
                {
                    ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                    ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
                }
                catch (Exception)
                {

                    return Redirect("~/User/Company");
                }
                string nme = ViewBag.Comp.ToString();
                double potensafari = 0;
                try
                {
                    potensafari = db2.Account_Creation.Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created >= value).Count();

                }
                catch (Exception)
                {

                }
                double potenairtel = 0;
                try
                {
                    potenairtel = db2.Account_Creation.Where(a => a.MNO_ == "AIRTEL" && a.Date_Created >= value).Count();

                }
                catch (Exception)
                {

                  
                }
                double potenpesa = 0;
                try
                {
  potenpesa =db2.Account_Creation.Where(a => a.MNO_ == "PESALINK" && a.Date_Created >= value).Count();
              
                }
                catch (Exception f)
                {

                }
                if (nme.Contains("OLD"))
                {
                    try
                    {
potensafari = db2.Account_Creation.Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created < value).Count();

                    }
                    catch (Exception f)
                    {

                    }
                    try
                    {
 potenairtel = db2.Account_Creation.Where(a => a.MNO_ == "AIRTEL" && a.Date_Created < value).Count();
                   
                    }
                    catch (Exception f)
                    {

                    }
                    try
                    {
     potenpesa = db2.Account_Creation.Where(a => a.MNO_ == "PESALINK" && a.Date_Created < value).Count();
               
                    }
                    catch (Exception f)
                    {

                    }
                }
                if (nme.Contains("LATEST"))
                {
                    try
                    {
                        potensafari = db2.Account_Creation.Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created>=value2 && a.Date_Created >= value3).Count();

                    }
                    catch (Exception f)
                    {

                    }
                    try
                    {
                        potenairtel = db2.Account_Creation.Where(a => a.MNO_ == "AIRTEL" && a.Date_Created >= value2 && a.Date_Created >= value3).Count();

                    }
                    catch (Exception f)
                    {

                    }
                    try
                    {
                        potenpesa = db2.Account_Creation.Where(a => a.MNO_ == "PESALINK" && a.Date_Created >= value2 && a.Date_Created >= value3).Count();

                    }
                    catch (Exception f)
                    {

                    }
                }
                //double potenpesa = 10000000.00; //db2.Account_Creation.ToList().Where(a => a.MNO_ == "PESALINK" && a.Active == true).Count();

                var db = Database.Open("Mkiba2");

                var selectQueryString = "SELECT * from Bond_Payment_FailedBids";
                double actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)").ToList().Count();

                if (nme.Contains("OLD"))
                {
                    actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme.Replace("OLD", "") + "'  and CAST(Date_Created as date)<cast('25-FEB-2019' as date)").ToList().Count();

                }
                if (nme.Contains("LATEST"))
                {
                    actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme.Replace("LATEST", "") + "'  and CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date)").ToList().Count();

                }
                if (nme== "KE5000006766")
                {
                    actualsafari = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme.Replace("OLD", "") + "'").ToList().Count();

                }

                double actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)").Count();
                if (nme.Contains("OLD"))
                {
                    actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme.Replace("OLD", "") + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)").Count();

                }
                if (nme.Contains("LATEST"))
                {
                    actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme.Replace("LATEST", "") + "' and CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date)").Count();

                }
                if (nme== "KE5000006766")
                {
                    actualairtel = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme.Replace("OLD", "") + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)").Count();

                }
                double actualpesa = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "'").Count();
                if (nme.Contains("OLD"))
                {
                    actualpesa = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme.Replace("OLD", "") + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)").Count();

                }
                if (nme.Contains("LATEST"))
                {
                    actualpesa = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme.Replace("OLD", "") + "' and CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date) ").Count();

                }
                if (nme== "KE5000006766")
                {
                    actualpesa = db.Query("select distinct CDSC_Number as 'Total' from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme.Replace("OLD", "") + "'").Count();

                }
                ViewBag.PS = potensafari.ToString("#,##");
                ViewBag.PA = potenairtel.ToString("#,##0");
                ViewBag.PE = potenpesa.ToString("#,##0");
                ViewBag.AS = actualsafari.ToString("#,##0");
                ViewBag.AA = actualairtel.ToString("#,##0");
                ViewBag.AP = actualpesa.ToString("#,##0");

                float c1 = 0;
                try
                {
  c1=float.Parse(((actualsafari / potensafari) * 100).ToString(), CultureInfo.InvariantCulture.NumberFormat);

               }
            catch (Exception f)
            {

            }
            float c2 = 0;
                try
                {
                    c2 = float.Parse(((actualairtel / potenairtel) * 100).ToString(), CultureInfo.InvariantCulture.NumberFormat);
                }
                catch (Exception f)
                {

                }
                float c3 = 0;
                try
                {
c3=float.Parse(((actualpesa / potenpesa) * 100).ToString(), CultureInfo.InvariantCulture.NumberFormat);

                }
                catch (Exception f)
                {

                }
                
                ViewBag.CS = Math.Round(c1, 2).ToString();
                ViewBag.CA = Math.Round(c2, 2).ToString();
                ViewBag.PSS = Math.Round(c3, 2).ToString();
                int regcount = 0;
                try
                {
regcount = db2.Account_Creation.Where(a=>a.Date_Created>=value).Count();
                if (nme.Contains("OLD"))
                {
                regcount = db2.Account_Creation.Where(a => a.Date_Created<value).Count();
                }
                }
                catch (Exception f)
                {
                 
                }
                
                string dt = DateTime.Now.ToString("dd/MM/yyyy");
                int regcount2 = 0;
                try
                {
                    regcount2 = db2.Account_Creation.Where(a => a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy")).Count();

                }
                catch (Exception f)
                {

                }
                int by = 0;
                try
                {
                    by = (from c in db2.Bond_Payment_Audit
                          where c.Company==nme && c.Date_Created>=value
                          select new { c.CDSC_Number }).Distinct().Count();
                    if (nme.Contains("OLD"))
                    {
                        nme = nme.Replace("OLD", "");
                        by = (from c in db2.Bond_Payment_Audit
                              where c.Company == nme && c.Date_Created < value
                              select new { c.CDSC_Number }).Distinct().Count();
                    }
                    if (nme.Contains("LATEST"))
                    {
                        nme = nme.Replace("LATEST","");
                        by = (from c in db2.Bond_Payment_Audit
                              where c.Company == nme && c.Date_Created >= value2 && c.Date_Created <= value3
                              select new { c.CDSC_Number }).Distinct().Count();
                    }
                    if (nme== "KE5000006766")
                    {
                        by = (from c in db2.Bond_Payment_Audit
                              where c.Company == nme
                              select new { c.CDSC_Number }).Distinct().Count();
                    }

                }
                catch(Exception)
                {

                }
               
                double per = 0;

                try
                {
                    per=((Convert.ToDouble(by) / (Convert.ToDouble(regcount)))) * 100;
                }
                catch (Exception f)
                {
                    per = 0;
                }


                ViewBag.PC = Math.Round(per, 2).ToString();
                decimal? overall = 0;
                try
                {
                    overall = db2.Bond_Payment_Audit.Where(a => a.Company == nme && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);

                }
                catch (Exception)
                {

                   
                }
                if (nme.Contains("OLD"))
                {
                    nme = nme.Replace("OLD", "");
                    overall = db2.Bond_Payment_Audit.Where(a => a.Company == nme.Replace("OLD", "") && a.Date_Created <= value).Sum(a => a.No_of_Notes_Applied);
                }
                if (nme.Contains("LATEST"))
                {
                    nme = nme.Replace("LATEST", "");
                    overall = db2.Bond_Payment_Audit.Where(a => a.Company == nme.Replace("OLD", "") && a.Date_Created >= value2 && a.Date_Created <= value3).Sum(a => a.No_of_Notes_Applied);
                }
                if (nme== "KE5000006766")
                {
                    overall = db2.Bond_Payment_Audit.Where(a => a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);
                }
                decimal? overallsafari = 0;
                try
                {
                    overallsafari =db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);
              
                }
                catch (Exception f)
                {

                }
                decimal? overallairtel = 0;
                try
                {
                    overallairtel=db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);
               
                }
                catch (Exception f)
                {

                }
                decimal? overallpesa = 0;
                try
                {
  overallpesa=db2.Bond_Payment_Audit.Where(a => a.MNO_ == "PESALINK" && a.Company == nme && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);
              
                }
                catch (Exception f)
                {

                }
                if (nme.Contains("OLD"))
                {
                    try
                    {
                        overallsafari = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created < value).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }

                    try
                    {
                        overallairtel = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "") && a.Date_Created < value).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        overallpesa = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "PESALINK" && a.Company == nme && a.Date_Created < value).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }
                }
                if (nme== "KE5000006766")
                {
                    try
                    {
                        overallsafari = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }

                    try
                    {
                        overallairtel = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        overallpesa = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "PESALINK" && a.Company == nme ).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                   }
                }
                if (nme.Contains("LATEST"))
                {
                    try
                    {
                        overallsafari = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("LATEST", "") && a.Date_Created >= value2 && a.Date_Created >= value3).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }

                    try
                    {
                        overallairtel = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("LATEST", "") && a.Date_Created >= value2 && a.Date_Created >= value3).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        overallpesa = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "PESALINK" && a.Company == nme.Replace("LATEST", "") && a.Date_Created>=value2 && a.Date_Created >= value3).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }
                }
                decimal? overallt = 0;
                    try
                    {
                        overallt = db2.Bond_Payment_Audit.Where(a => a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy") && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f)
                    {

                    }
                  
                    decimal? overallsafarit = 0;
                    try
                    {
                        overallsafarit = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy") && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception)
                    {

                    }
                    decimal? overallairtelt = 0;
                    try
                    {
                        overallairtelt = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy") && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);

                    }
                    catch (Exception f){

                    }
                 
                    decimal? overallpesat = 0;
                    try
                    {
                        overallpesat = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "PESALINK" && a.Date_Created.Value.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy") && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);


                    }
                    catch (Exception f)
                    {

                    }

                    ViewBag.OO = Convert.ToDouble(overall).ToString("#,##0.00");
                    ViewBag.OS = Convert.ToDouble(overallsafari).ToString("#,##0.00");
                    ViewBag.OA = Convert.ToDouble(overallairtel).ToString("#,##0.00");
                    ViewBag.OP = Convert.ToDouble(overallpesa).ToString("#,##0.00");

                    ViewBag.OOT = Convert.ToDouble(overallt).ToString("#,##0.00");
                    ViewBag.OST = Convert.ToDouble(overallsafarit).ToString("#,##0.00");
                    ViewBag.OAT = Convert.ToDouble(overallairtelt).ToString("#,##0.00");
                    ViewBag.OAP = Convert.ToDouble(overallpesat).ToString("#,##0.00");
                
            }catch(Exception f)
            {

            }
            return View();
        }

        public ActionResult Index3()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            try
            {
                string nme = ViewBag.Comp.ToString();

                int failedreg = db2.AccountC_FailedReg.ToList().Count();
                var db = Database.Open("Mkiba2");

                var fail = db.Query("select IsNull(COUNT(ReceiptNumber),'0') as 'CC' from Bond_Payment_FailedBids where CAST(Date_Created as date)<cast('25-FEB-2019' as date)").ToList();
                var sf = db.Query("select sum(TRY_CONVERT(MONEY,AmountPaid)) as 'Total' from Bond_Payment_FailedBids where CAST(Date_Created as date)<cast('25-FEB-2019' as date)").ToList();
                if (nme.Contains("OLD"))
                {
                  fail = db.Query("select IsNull(COUNT(ReceiptNumber),'0') as 'CC' from Bond_Payment_FailedBids where CAST(Date_Created as date)<cast('25-FEB-2019' as date)").ToList();
                 sf = db.Query("select sum(TRY_CONVERT(MONEY,AmountPaid)) as 'Total' from Bond_Payment_FailedBids where CAST(Date_Created as date)<cast('25-FEB-2019' as date)").ToList();

                }
                if (nme.Contains("LATEST"))
                {
                    fail = db.Query("select IsNull(COUNT(ReceiptNumber),'0') as 'CC' from Bond_Payment_FailedBids where CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date)").ToList();
                    sf = db.Query("select sum(TRY_CONVERT(MONEY,AmountPaid)) as 'Total' from Bond_Payment_FailedBids where CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date)").ToList();

                }
                string vf = "";
                string ff = "";

                foreach (var d in sf)
                {
                    vf = d.Total.ToString();
                }
                foreach (var q in fail)
                {
                    ff = q.CC.ToString();
                }

                string comfR = "", Rerr = "";
                try
                {

                    var commonreg = db.Query("Exec CommonError22").ToList();

                    foreach (var d in commonreg)
                    {
                        comfR = d.Reason.ToString();
                        Rerr = d.RError.ToString();
                    }
                }
                catch (Exception)
                {


                }

                string comfRB = "", RerrB = "";
                var commonbid = db.Query("select top 1 Reason,IsNull(Count(ReceiptNumber),'0') as 'RError' from Bond_Payment_FailedBids CAST(Date_Created as date)>=cast('25-FEB-2019' as date) group by Reason order by Count(ReceiptNumber) desc").ToList();
                if (nme.Contains("OLD"))
                {
             commonbid = db.Query("select top 1 Reason,IsNull(Count(ReceiptNumber),'0') as 'RError' from Bond_Payment_FailedBids where CAST(Date_Created as date)<cast('25-FEB-2019' as date) group by Reason order by Count(ReceiptNumber) desc").ToList();

                }
                if (nme.Contains("LATEST"))
                {
                    commonbid = db.Query("select top 1 Reason,IsNull(Count(ReceiptNumber),'0') as 'RError' from Bond_Payment_FailedBids where CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date) group by Reason order by Count(ReceiptNumber) desc").ToList();

                }
                foreach (var d in commonbid)
                {
                    comfRB = d.Reason.ToString();
                    RerrB = d.RError.ToString();
                }
                decimal? avgR = 0;
                DateTime value = new DateTime(2019,2, 25);
                DateTime value2 = new DateTime(2019,5, 25);
                DateTime value3 = new DateTime(2019,6, 8);
                try
                {
                    avgR = db2.AccountC_FailedReg.Where(a=>a.Date_Created>=value).Average(a => a.AmountPaid);
                    if (nme.Contains("OLD"))
                    {
                        avgR = db2.AccountC_FailedReg.Where(a => a.Date_Created<value).Average(a => a.AmountPaid);

                    }
                    if (nme.Contains("LATEST"))
                    {
                        avgR = db2.AccountC_FailedReg.Where(a => a.Date_Created >= value2 && a.Date_Created <= value3).Average(a => a.AmountPaid);

                    }
                }
                catch (Exception)
                {

                    avgR = 0;
                }
                try
                {
                    ViewBag.FailedReg = failedreg.ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.FailedReg = "0";
                }
                try
                {
                    ViewBag.FB = Math.Round(Convert.ToDecimal(ff), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.FB = "0";
                }
                ViewBag.CommonError = comfR.ToString();
                try
                {
                    ViewBag.CommonOccur = Convert.ToDouble(Rerr).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.CommonOccur = "0";
                }
                ViewBag.CommonErrorB = comfRB.ToString();
                try
                {
                    ViewBag.CommonOccurB = Convert.ToDouble(RerrB).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.CommonOccurB = "0";
                }
                try
                {
                    ViewBag.SF = Math.Round(Convert.ToDecimal(vf), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.SF = "0";
                }
                try
                {
                    ViewBag.AVR = Math.Round(Convert.ToDecimal(avgR), 2).ToString("#,##0.00");

                }
                catch (Exception)
                {

                    ViewBag.AVR = "0";
                }
                try
                {
                    //var sfv = db.Query("select IsNull(AVG(CAST(AmountPaid As money)),'0') As 'Total' from Bond_Payment_FailedBids").ToList();
                    //string vfv = "";
                    //foreach (var d in sfv)
                    //{
                    //    vfv = d.Total.ToString();
                    //}
                    //ViewBag.FAV = Math.Round(Convert.ToDecimal(vfv), 2).ToString();

                }
                catch (Exception)
                {

                    ViewBag.FAV = "0";

                }
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');

                //var sfc = db.Query("select IsNull(Reason,'None') As 'Reason',Date_Created,IsNull(AmountPaid,'0') As 'AmountPaid' from Bond_Payment_FailedBids").ToList();
                //var client = new RestClient(baseUrl);
                //var request = new RestRequest("failedbid", Method.GET);
                //IRestResponse response = client.Execute(request);
                // string validate = response.Content;
                List<FailedBids> dataList = falls();



                try
                {
                    ViewBag.SFP = dataList.ToList();
                }
                catch (Exception)
                {

                    ViewBag.SFP = null;
                }

            }
            catch (Exception)
            {


            }
            return View();
        }

        public List<FailedBids> falls()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            //return listEmp.First(e => e.ID == id); 
            string nme = ViewBag.Comp.ToString();
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids  where CAST(Date_Created as date)>=cast('25-FEB-2019' as date) order by Date_Created desc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");
                sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids  where CAST(Date_Created as date)<cast('25-FEB-2019' as date) order by Date_Created desc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                sqlCmd.CommandText = "select top 20 Reason,IsNull(AmountPaid,'0') as 'AmountPaid',Date_Created from Bond_Payment_FailedBids  where CAST(Date_Created as date)>=cast('25-May-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date) order by Date_Created desc";

            }

            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<FailedBids>();
            try
            {

            while (reader.Read())
            {
                var accountDetails = new FailedBids
                {

                    Reason = reader.GetValue(0).ToString(),
                    AmountPaid = reader.GetValue(1).ToString(),
                    Date_Created = Convert.ToDateTime(reader.GetValue(2)).ToString("dd/MM/yyyy:HH:mm:ss")
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            }
            catch (Exception e)
            {

            }
            return accDetails;
        }

        public List<DailyTrade> Safari()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD","");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)<CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('08-June-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
          
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a   group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<DailyTrade> Safari2(string dt, string dt2)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }

            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-FEB-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('25-FEB-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }

            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('08-June-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='SAFARICOM' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='SAFARICOM' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='SAFARICOM'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            }
            catch (Exception e)
            {

            }
            return accDetails;
        }

        public List<DailyTrade> Airtel()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', IsNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "'  and CAST(Date_Created as date)>=CAST('25-FEB-2019' as date)),0) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-FEB-2019' as date) and CAST(a.Date_Created as date)<=CAST(getdate() as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "'  and CAST(Date_Created as date)<CAST('25-FEB-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)<=CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "'  and CAST(Date_Created as date)>=CAST('25-May-2019' as date) and CAST(Date_Created as date)<=CAST('08-june-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('08-June-2019' as date) group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

           
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<DailyTrade> Airtel2(string dt, string dt2)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }

            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-FEB-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");


                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");

                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='AIRTEL' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='AIRTEL' and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='AIRTEL'  and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

           
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }

        //PesaLink


        public List<DailyTrade> PESA()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "'  and CAST(Date_Created as date)>=CAST('25-FEB-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");

                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<CAST('25-FEB-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)<CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");

                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST('25-May-2019' as date) and CAST(Date_Created as date)<=CAST('08-June-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('08-June-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<DailyTrade> PESA2(string dt, string dt2)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }

            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK') as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "'  and CAST(Date_Created as date)>=CAST('25-FEB-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-FEB-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");

                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<CAST('25-FEB-2019' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");

                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)) as 'Average Bid'  from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            if (nme== "KE5000006766")
            {
                sqlCmd.CommandText = "SET ARITHABORT OFF   SET ANSI_WARNINGS OFF select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where MNO_='PESALINK'  and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 ,0) as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "' and CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where MNO_='PESALINK' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where MNO_='PESALINK' and Company='" + nme + "') as 'Average Bid'  from Bond_Payment_Audit a group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTrade>();
            try
            {

          
            while (reader.Read())
            {
                var accountDetails = new DailyTrade
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<DailyTradeFull> Makiba()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 

            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='"+ nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Average Bid' ,(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2017-03-23' as date) and CAST(a.Date_Created as date)<=CAST('2017-03-30' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))),0))*100,0) as 'Percentage of Bond Taken Up',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',IsNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',IsNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=CAST('25-FEB-2019' as date)) as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");

                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))),0))*100,0) as 'Percentage of Bond Taken Up',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',IsNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',IsNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<CAST('25-FEB-2019' as date)) as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)<CAST('2019-02-25' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");

                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))),0))*100,0) as 'Percentage of Bond Taken Up',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',IsNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',IsNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<CAST('25-FEB-2019' as date)) as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-MAY-2019' as date) and CAST(a.Date_Created as date)<=CAST('08-June-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }
            if (nme== "KE5000006766")
            {
                nme = nme.Replace("OLD", "");

                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',IsNull(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date))),0))*100,0) as 'Percentage of Bond Taken Up',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',IsNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',IsNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations',(select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where Company='" + nme + "') as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a   group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }


            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<DailyTradeFull>();
            try
            {

           
            while (reader.Read())
            {
                var accountDetails = new DailyTradeFull
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString()),

                    averagePer = Convert.ToDecimal(reader.GetValue(10).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }


        public List<DailyTradeFull> Makiba2(string dt, string dt2)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            //return listEmp.First(e => e.ID == id); 
            string nme = ViewBag.Comp.ToString();
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            //sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Buys Per day (IPO)',((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))))*100 as 'Percentage of Bond Taken Up',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Registrations',(Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)) as 'Registrations Per Day',(select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date)) as 'Total No. of Bids',(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Total Buyers',CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date))/(Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)))*100 as decimal(18,2)) as 'Percentage Of Buyers To Registrations', (select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit)/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit )/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'Average Bid' ,(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date))/(select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',ISnULL(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/(NullIf((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0)))*100 ,0) as 'Percentage of Bond Taken Up',ISNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',isNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',isNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-FEB-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");
                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',ISnULL(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/(NullIf((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0)))*100 ,0) as 'Percentage of Bond Taken Up',ISNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',isNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',isNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('" + dt + "' as date) and CAST(a.Date_Created as date)<CAST('25-FEB-2019' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',ISnULL(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/(NullIf((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0)))*100 ,0) as 'Percentage of Bond Taken Up',ISNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',isNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',isNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a where CAST(a.Date_Created as date)>=CAST('25-May-2019' as date) and CAST(a.Date_Created as date)<=CAST('" + dt2 + "' as date)  group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }
            if (nme== "KE5000006766")
            {
                nme = nme.Replace("OLD", "");
                sqlCmd.CommandText = "select distinct CAST(Date_Created as date) 'Day',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'TotalBuyCumulative',(select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Buys Per day (IPO)',ISnULL(((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/(NullIf((select isnull(sum(AmountPaid),'0') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0)))*100 ,0) as 'Percentage of Bond Taken Up',ISNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0) as 'Total Registrations',isNull((Select count(CDSC_Number) from Account_Creation where  CAST(Date_Created as date)=CAST(a.Date_Created as date)),0) as 'Registrations Per Day',isNull((select count(CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)>=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total No. of Bids',isNull((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0) as 'Total Buyers',isNull(CAST(((Select  CAST( count(DISTINCT CDSC_Number) as decimal(18,1)) from Bond_Payment_Audit where  CAST(Date_Created as date)=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((Select CAST(count(CDSC_Number) as decimal(18,1)) from Account_Creation where  CAST(Date_Created as date)<=CAST(a.Date_Created as date)),0))*100 as decimal(18,2)),0) as 'Percentage Of Buyers To Registrations', (select IsNuLL(AVG(AmountPaid),0) from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "') as 'Average Bid' ,isNull((select isnull(sum(AmountPaid),'0')  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "')/NULLIF((select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where  CAST(Date_Created as date)<=CAST(a.Date_Created as date) and Company='" + nme + "'),0),0) as 'AvaragePerInvestor' from Bond_Payment_Audit a   group by CAST(Date_Created as date)  order by CAST(Date_Created as date) asc";


            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
           
        
            var accDetails = new List<DailyTradeFull>();
            try
            {

            while (reader.Read())
            {
                var accountDetails = new DailyTradeFull
                {
                    Day = Convert.ToDateTime(reader.GetValue(0).ToString()),
                    TotalBuyC = Convert.ToDecimal(reader.GetValue(1).ToString()),
                    BuysPerDay = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    percentageBond = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    TotalRegistrations = Convert.ToInt64(reader.GetValue(4).ToString()),

                    RegistrationsP = Convert.ToInt64(reader.GetValue(5).ToString()),

                    totalBids = Convert.ToInt64(reader.GetValue(6).ToString()),

                    totalBuyers = Convert.ToInt64(reader.GetValue(7).ToString()),

                    BPR = Convert.ToDecimal(reader.GetValue(8).ToString()),

                    averageBid = Convert.ToDecimal(reader.GetValue(9).ToString()),

                    averagePer = Convert.ToDecimal(reader.GetValue(10).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }


        public ActionResult Index4()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }

            return View();
        }
        public List<Investor> fallsz()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();
            //return listEmp.First(e => e.ID == id); 
            string my = @"select top 20 CDSC_Number as 'Account',Surname_CompanyName+' '+OtherNames As 'Name',MNO_ as 'MNO', (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) as 'Invested Amount' from Account_Creation where CAST(Date_Created as date)>=cast('25-FEB-2019' as date) order by (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) desc";
            if (my.Contains("OLD"))
            {
                nme = nme.Replace("OLD","");
                my = @"select top 20 CDSC_Number as 'Account',Surname_CompanyName+' '+OtherNames As 'Name',MNO_ as 'MNO', (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) as 'Invested Amount' from Account_Creation where CAST(Date_Created as date)<cast('25-FEB-2019' as date)  order by (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) desc";

            }
            if (my.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                my = @"select top 20 CDSC_Number as 'Account',Surname_CompanyName+' '+OtherNames As 'Name',MNO_ as 'MNO', (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) as 'Invested Amount' from Account_Creation where CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-June-2019' as date)  order by (select isnull(sum(AmountPaid),0) from Bond_Payment_Audit where CDSC_Number=Account_Creation.CDSC_Number) desc";

            }
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = my;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Investor>();
            try
            {

           
            while (reader.Read())
            {
                var accountDetails = new Investor
                {
                    Account = reader.GetValue(0).ToString(),
                    Name = reader.GetValue(1).ToString(),

                    MNO = reader.GetValue(2).ToString(),

                    InvestedAmount = Convert.ToDecimal(reader.GetValue(3).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<AnalysisByBand> fallsno()
        {
            //return listEmp.First(e => e.ID == id); 

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            string nme = ViewBag.Comp.ToString();

           string n1 = "select 'Minimum amount-Kshs. 3,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied <= 3000  and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) )/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
           string n2 = "union all select 'Kshs. 3,001-10000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
            string n3 = "union all select 'Kshs. 10001-20000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
           string n4 = "union all select 'Kshs. 20001-50000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
           string n5 = "union all select 'Kshs. 50,001-100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
          string  n6 = "union all select 'Above Kshs.100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
           string n7 = "union all select 'Total Investors' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where CAST(Date_Created as date)>=cast('25-FEB-2019' as date) ))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)>=cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");

              n1 = "select 'Minimum amount-Kshs. 3,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied <= 3000  and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) ) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) )/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
                 n2 = "union all select 'Kshs. 3,001-10000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n3 = "union all select 'Kshs. 10001-20000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
               n4 = "union all select 'Kshs. 20001-50000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n5 = "union all select 'Kshs. 50,001-100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n6 = "union all select 'Above Kshs.100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n7 = "union all select 'Total Investors' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where CAST(Date_Created as date)<cast('25-FEB-2019' as date) ))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as decimal(18,2)) as 'Percentage Of Value'";

            }
            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");

                n1 = "select 'Minimum amount-Kshs. 3,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company = '" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) )))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied <= 3000  and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) ) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)) )/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";

                n2 = "union all select 'Kshs. 3,001-10000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";

                n3 = "union all select 'Kshs. 10001-20000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";

                n4 = "union all select 'Kshs. 20001-50000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";

                n5 = "union all select 'Kshs. 50,001-100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date) ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)) )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";

                n6 = "union all select 'Above Kshs.100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company = '" + nme + "'))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' )))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company = '" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company = '" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "')))*100 as decimal(18, 2)) as 'Percentage Of Value'";
                n7 = "union all select 'Total Investors' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date)) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-May-2019' as date) and cast(Date_Created as date) <=cast('08-June-2019' as date)))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company = '" + nme + "' and CAST(Date_Created as date)>= cast('25-May-2019' as date) and cast(Date_Created as date)<=cast('08-June-2019' as date))))*100 as decimal(18, 2)) as 'Percentage Of Value'";
            }
            if (nme== "KE5000006766")
            {
                nme = nme.Replace("OLD", "");

                n1 = "select 'Minimum amount-Kshs. 3,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "') as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied <= 3000  and Company='" + nme + "' ) as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied <= 3000 and Company='" + nme + "') )/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n2 = "union all select 'Kshs. 3,001-10000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "' ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied<= 10000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 3001 and No_of_Notes_Applied <= 10000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n3 = "union all select 'Kshs. 10001-20000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "' ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied<= 20000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 10001 and No_of_Notes_Applied <= 20000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n4 = "union all select 'Kshs. 20001-50000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "') as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied<= 50000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 20001 and No_of_Notes_Applied <= 50000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n5 = "union all select 'Kshs. 50,001-100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "' ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "') )/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied<= 100000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied >= 50001 and No_of_Notes_Applied <= 100000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n6 = "union all select 'Above Kshs.100,000' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 ) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "' )))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where No_of_Notes_Applied > 100000 and Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";
                n7 = "union all select 'Total Investors' as 'Amount analysis as band',(select count(No_of_Notes_Applied) from Bond_Payment_Audit where CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Number',CAST((((select CAST(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where CAST(Date_Created as date)<cast('25-FEB-2019' as date) ))/ ((select cast(count(No_of_Notes_Applied) as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage',(select isnull(sum(No_of_Notes_Applied), '0.00') from Bond_Payment_Audit where Company='" + nme + "') as 'Value',CAST((((select cast(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "'))/ ((select CAST(isnull(sum(No_of_Notes_Applied), '0.00') as decimal) from Bond_Payment_Audit where Company='" + nme + "')))*100 as decimal(18,2)) as 'Percentage Of Value'";

            }
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = n1 + n2 + n3 + n4 + n5 + n6 + n7;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<AnalysisByBand>();
            try
            {

          
            while (reader.Read())
            {
                var accountDetails = new AnalysisByBand
                {
                    analysisband = reader.GetValue(0).ToString(),
                    Number = Convert.ToInt64(reader.GetValue(1).ToString()),

                    Percentage = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    Value = Convert.ToDecimal(reader.GetValue(3).ToString()),

                    PercentageValue = Convert.ToDecimal(reader.GetValue(4).ToString())
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public List<CumulativeBuys> CumulativeBu()
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                Response.Redirect("~/User/Company");
            }
            //return listEmp.First(e => e.ID == id); 
            string nme = ViewBag.Comp.ToString();
            //Cumulative Buys
            string n1 = "select 'Total Buys Cumulative' as 'c', (select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'Safaricom',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as 'Airtel',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))+(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))  as 'Total'";
            string n2 = "union all select 'Percentage Of Bond Taken up' as 'c', ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 As 'Safaricom',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 as 'Airtel',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))))*100 as 'Total'";
            string n3 = "union all select 'Total Registrations' as 'c',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'SAFARICOM',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'AIRTEL',(Select count(CDSC_Number) from Account_Creation where MNO_ not in('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'unallocated', (Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))  as 'Totals'";
            string n4 = "union all select 'Total No. Bids' as 'c',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'Safaricom',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as 'Airtel',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS 'Unallocated',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as 'Totals'";
            string n5 = "union all select 'Total Buyers' as 'c',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'SAFARICOM',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as 'AIRTEL',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as 'Unallocated',(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date))+(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) As 'Totals'";
            string n6 = "union all select 'Percentage of Buyers to Registrations' as 'c',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) As 'SAFARICOM',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'AIRTEL',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_  not in ('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'Unallocated', CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) +CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2))+CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'Totals'";
            string n7 = "union all select 'Average Bid' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2)) As 'Safaricom',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2)) as 'Airtel',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2)) As 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) as decimal(18, 2))  as 'Totals'";
            string n8 = "union all select 'Average Per Investor' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2)) As 'SAFARICOM',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2)) as 'AIRTEL',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ not in('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2)) as 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2))+CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)>= cast('25-FEB-2019' as date)) AS decimal(18, 2)) As 'Totals'";

            if (nme.Contains("OLD"))
            {
                nme = nme.Replace("OLD", "");
                n1 = "select 'Total Buys Cumulative' as 'c', (select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'Safaricom',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Airtel',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))+(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))  as 'Total'";
                n2 = "union all select 'Percentage Of Bond Taken up' as 'c', ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 As 'Safaricom',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as 'Airtel',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))))*100 as 'Total'";
                n3 = "union all select 'Total Registrations' as 'c',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'SAFARICOM',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'AIRTEL',(Select count(CDSC_Number) from Account_Creation where MNO_ not in('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'unallocated', (Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)<cast('25-FEB-2019' as date))  as 'Totals'";
               n4 = "union all select 'Total No. Bids' as 'c',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'Safaricom',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Airtel',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS 'Unallocated',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Totals'";
                n5 = "union all select 'Total Buyers' as 'c',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'SAFARICOM',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'AIRTEL',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as 'Unallocated',(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date))+(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) As 'Totals'";
                n6 = "union all select 'Percentage of Buyers to Registrations' as 'c',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) As 'SAFARICOM',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'AIRTEL',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_  not in ('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'Unallocated', CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) +CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2))+CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)<cast('25-FEB-2019' as date)))*100 as decimal(18, 2)) as 'Totals'";
               n7 = "union all select 'Average Bid' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2)) As 'Safaricom',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2)) as 'Airtel',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2)) As 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) as decimal(18, 2))  as 'Totals'";
              n8 = "union all select 'Average Per Investor' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2)) As 'SAFARICOM',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2)) as 'AIRTEL',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ not in('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2)) as 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2))+CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL', 'SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date) >= cast('25-FEB-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '"+ nme +"' and CAST(Date_Created as date)<cast('25-FEB-2019' as date)) AS decimal(18, 2)) As 'Totals'";


            }

            if (nme.Contains("LATEST"))
            {
                nme = nme.Replace("LATEST", "");
                n1 = "select 'Total Buys Cumulative' as 'c', (select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'Safaricom',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as 'Airtel',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))+(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))  as 'Total'";
                n2 = "union all select 'Percentage Of Bond Taken up' as 'c', ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 As 'Safaricom',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 as 'Airtel',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))))*100 as 'Total'";
                n3 = "union all select 'Total Registrations' as 'c',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'SAFARICOM',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'AIRTEL',(Select count(CDSC_Number) from Account_Creation where MNO_ not in('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'unallocated', (Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(Select count(CDSC_Number) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))  as 'Totals'";
                n4 = "union all select 'Total No. Bids' as 'c',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'Safaricom',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as 'Airtel',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS 'Unallocated',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as 'Totals'";
                n5 = "union all select 'Total Buyers' as 'c',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'SAFARICOM',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as 'AIRTEL',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as 'Unallocated',(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date))+(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) As 'Totals'";
                n6 = "union all select 'Percentage of Buyers to Registrations' as 'c',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2)) As 'SAFARICOM',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2)) as 'AIRTEL',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_  not in ('SAFARICOM', 'AIRTEL') and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2)) as 'Unallocated', CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2)) +CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2))+CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL') and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)))*100 as decimal(18, 2)) as 'Totals'";
                n7 = "union all select 'Average Bid' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2)) As 'Safaricom',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2)) as 'Airtel',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2)) As 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) as decimal(18, 2))  as 'Totals'";
                n8 = "union all select 'Average Per Investor' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2)) As 'SAFARICOM',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2)) as 'AIRTEL',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ not in('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2)) as 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2))+CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL', 'SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date) >= cast('25-MAY-2019' as date)) / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "' and CAST(Date_Created as date)>=cast('25-MAY-2019' as date) and CAST(Date_Created as date)<=cast('08-JUN-2019' as date)) AS decimal(18, 2)) As 'Totals'";


            }
            if (nme== "KE5000006766")
            {
                nme = nme.Replace("OLD", "");
                n1 = "select 'Total Buys Cumulative' as 'c', (select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') As 'Safaricom',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') as 'Airtel',(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "'))+(select isnull(sum(No_of_Notes_Applied), '0') As 'IssuedQuantity' from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "')  as 'Total'";
                n2 = "union all select 'Percentage Of Bond Taken up' as 'c', ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "')))*100 As 'Safaricom',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')))*100 as 'Airtel',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "')))*100 As 'Unallocated',((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "')))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')))*100 + ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "')/ ((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') +(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "')))*100 as 'Total'";
                n3 = "union all select 'Total Registrations' as 'c',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM') As 'SAFARICOM',(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL') As 'AIRTEL',(Select count(CDSC_Number) from Account_Creation where MNO_ not in('SAFARICOM', 'AIRTEL')) As 'unallocated', (Select count(CDSC_Number) from Account_Creation where MNO_ = 'SAFARICOM')+(Select count(CDSC_Number) from Account_Creation where MNO_ = 'AIRTEL')+(Select count(CDSC_Number) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL'))  as 'Totals'";
                n4 = "union all select 'Total No. Bids' as 'c',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') As 'Safaricom',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') as 'Airtel',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') AS 'Unallocated',(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "')+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(select count(CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "') as 'Totals'";
                n5 = "union all select 'Total Buyers' as 'c',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') As 'SAFARICOM',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') as 'AIRTEL',(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') as 'Unallocated',(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "')+(Select  count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "')+(Select count(DISTINCT CDSC_Number) from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') As 'Totals'";
                n6 = "union all select 'Percentage of Buyers to Registrations' as 'c',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM'))*100 as decimal(18, 2)) As 'SAFARICOM',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL'))*100 as decimal(18, 2)) as 'AIRTEL',CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_  not in ('SAFARICOM', 'AIRTEL')))*100 as decimal(18, 2)) as 'Unallocated', CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'SAFARICOM'))*100 as decimal(18, 2)) +CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ = 'AIRTEL'))*100 as decimal(18, 2))+CAST(((Select distinct CAST(count(CDSC_Number) as decimal(18, 1)) from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') / (Select CAST(count(CDSC_Number) as decimal(18, 1)) from Account_Creation where MNO_ NOT IN('SAFARICOM','AIRTEL')))*100 as decimal(18, 2)) as 'Totals'";
                n7 = "union all select 'Average Bid' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') as decimal(18, 2)) As 'Safaricom',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') as decimal(18, 2)) as 'Airtel',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') as decimal(18, 2)) As 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') as decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ NOT IN('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') / (select count(DISTINCT CDSC_Number)  from Bond_Payment_Audit where MNO_ not in ('SAFARICOM', 'AIRTEL') and Company = '" + nme + "') as decimal(18, 2))  as 'Totals'";
                n8 = "union all select 'Average Per Investor' as 'c',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') AS decimal(18, 2)) As 'SAFARICOM',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') AS decimal(18, 2)) as 'AIRTEL',CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ not in ('AIRTEL', 'SAFARICOM') and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ not in('AIRTEL', 'SAFARICOM') and Company = '" + nme + "') AS decimal(18, 2)) as 'Unallocated',CAST((select isnull(sum(No_of_Notes_Applied), '0') from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'SAFARICOM' and Company = '" + nme + "') AS decimal(18, 2)) +CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ = 'AIRTEL' and Company = '" + nme + "') AS decimal(18, 2))+CAST((select isnull(sum(No_of_Notes_Applied), '0')  from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL', 'SAFARICOM') and Company = '" + nme + "') / (Select distinct count(CDSC_Number)from Bond_Payment_Audit where MNO_ NOT IN('AIRTEL','SAFARICOM') and Company = '" + nme + "') AS decimal(18, 2)) As 'Totals'";


            }
            string q1, q2;
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Mkiba2"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CumulativeBuys>();
            try
            {

          
            while (reader.Read())
            {
                var accountDetails = new CumulativeBuys
                {
                    c = reader.GetValue(0).ToString(),
                    Safaricom = Convert.ToDecimal(reader.GetValue(1).ToString()),

                    Airtel = Convert.ToDecimal(reader.GetValue(2).ToString()),

                    Unallocated= Convert.ToDecimal(reader.GetValue(3).ToString()),
                    Total = Convert.ToDecimal(reader.GetValue(4).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();
            }
            catch (Exception e)
            {

            }
            return accDetails;
        }
        public ActionResult Index5()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            return View();
        }

        public ActionResult Index6()
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }

            string nme = ViewBag.Comp.ToString();
            //Issued Quantity
            var cum = CumulativeBu();

            DateTime value = new DateTime(2019, 2, 25);
            DateTime value2 = new DateTime(2019, 5, 25);
            DateTime value3 = new DateTime(2019, 6, 08);
            ViewBag.ps = db2.Bond_Payment_Audit.Where(a => a.Date_Created >=value && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);
            if (nme.Contains("OLD"))
            {
                 ViewBag.ps = db2.Bond_Payment_Audit.Where(a=>a.Date_Created<value && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);
            }
            if (nme.Contains("LATEST"))
            {
                ViewBag.ps = db2.Bond_Payment_Audit.Where(a => a.Date_Created>=value2 && a.Date_Created <= value3 && a.Company == nme.Replace("OLD", "")).Sum(a => a.No_of_Notes_Applied);
            }
            if (nme == "KE5000006766")
            {

                ViewBag.ps = db2.Bond_Payment_Audit.Where(a =>a.Company == nme.Replace("OLD", "")).Sum(a => a.AmountPaid);

            }
            //
            //Safaricom Reconciliation
            ViewBag.sr = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);
            //Airtel Reconciliattion
            ViewBag.ar = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "") && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);

            ViewBag.pr = db2.Bond_Payment_Audit.Where(a => a.MNO_ != "AIRTEL" && a.MNO_!= "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created >= value).Sum(a => a.No_of_Notes_Applied);

            if (nme.Contains("OLD"))
            {
             
                ViewBag.sr = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created < value).Sum(a => a.No_of_Notes_Applied);
                //Airtel Reconciliattion
                ViewBag.ar = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "") && a.Date_Created < value).Sum(a => a.No_of_Notes_Applied);
                ViewBag.pr = db2.Bond_Payment_Audit.Where(a => a.MNO_ != "AIRTEL" && a.MNO_ != "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created<value).Sum(a => a.No_of_Notes_Applied);

            }
            if (nme.Contains("LATEST"))
            {

                ViewBag.sr = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created >=value2 && a.Date_Created <= value3).Sum(a => a.No_of_Notes_Applied);
                //Airtel Reconciliattion
                ViewBag.ar = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "") && a.Date_Created >= value2 && a.Date_Created <= value3).Sum(a => a.No_of_Notes_Applied);
                ViewBag.pr = db2.Bond_Payment_Audit.Where(a => a.MNO_ != "AIRTEL" && a.MNO_ != "SAFARICOM" && a.Company == nme.Replace("OLD", "") && a.Date_Created >= value2 && a.Date_Created <= value3).Sum(a => a.No_of_Notes_Applied);

            }
            if (nme== "KE5000006766")
            {

                ViewBag.sr = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "SAFARICOM" && a.Company == nme.Replace("OLD", "")).Sum(a => a.AmountPaid);
                //Airtel Reconciliattion
                ViewBag.ar = db2.Bond_Payment_Audit.Where(a => a.MNO_ == "AIRTEL" && a.Company == nme.Replace("OLD", "") ).Sum(a => a.AmountPaid);
                ViewBag.pr = db2.Bond_Payment_Audit.Where(a => a.MNO_ != "AIRTEL" && a.MNO_ != "SAFARICOM" && a.Company == nme.Replace("OLD", "") ).Sum(a => a.AmountPaid);

            }
            //
            ViewBag.analysis = fallsno();

            ViewBag.invs = fallsz();

            ViewBag.Cum = cum;
            return View();
        }

        public ActionResult Index7(string sortOrder, string currentFilter, string Date1String, string Date2String, int? page)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Safari()
                      select s;
            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(ViewBag.dt1) && !String.IsNullOrEmpty(ViewBag.dt2))
            {
                try
                {
                    System.Web.HttpContext.Current.Session["Date1"] = ViewBag.dt1;
                    System.Web.HttpContext.Current.Session["Date2"] = ViewBag.dt2;
                }
                catch (Exception)
                {

                }
                Date1String = ViewBag.dt1;
                Date2String = ViewBag.dt2;
            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;

                per = Safari2(Date1String, Date2String);
            }
            ViewBag.CurrentSort = sortOrder;
            //ViewBag.CurrentFilter = searchString;
            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.Day);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;
            return View(per.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult Index8(string sortOrder, string currentFilter, string Date1String, string Date2String, int? page)
        {

            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Airtel()
                      select s;
            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(ViewBag.dt1) && !String.IsNullOrEmpty(ViewBag.dt2))
            {
                try
                {
                    System.Web.HttpContext.Current.Session["Date1"] = ViewBag.dt1;
                    System.Web.HttpContext.Current.Session["Date2"] = ViewBag.dt2;
                }
                catch (Exception)
                {

                }
                Date1String = ViewBag.dt1;
                Date2String = ViewBag.dt2;
            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {

                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                per = Airtel2(Date1String, Date2String);
            }
            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.Day);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;
            return View(per.ToPagedList(pageNumber, pageSize));
        }



        public ActionResult Index9(string sortOrder, string currentFilter, string Date1String, string Date2String, int? page)
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in Makiba()
                      select s;
            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(ViewBag.dt1) && !String.IsNullOrEmpty(ViewBag.dt2))
            {
                try
                {
                    System.Web.HttpContext.Current.Session["Date1"] = ViewBag.dt1;
                    System.Web.HttpContext.Current.Session["Date2"] = ViewBag.dt2;
                }
                catch (Exception)
                {

                }
                Date1String = ViewBag.dt1;
                Date2String = ViewBag.dt2;
            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                per = Makiba2(Date1String, Date2String);
            }

            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.Day);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;
            return View(per.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Index10(string sortOrder, string currentFilter, string Date1String, string Date2String, int? page)
        {
            try
            {
                ViewBag.Comp = System.Web.HttpContext.Current.Session["safe"].ToString();
                ViewBag.CompN = System.Web.HttpContext.Current.Session["safe2"].ToString();
            }
            catch (Exception)
            {

                return Redirect("~/User/Company");
            }
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in PESA()
                      select s;
            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(ViewBag.dt1) && !String.IsNullOrEmpty(ViewBag.dt2))
            {
                try
                {
                    System.Web.HttpContext.Current.Session["Date1"] = ViewBag.dt1;
                    System.Web.HttpContext.Current.Session["Date2"] = ViewBag.dt2;
                }
                catch (Exception)
                {

                }
                Date1String = ViewBag.dt1;
                Date2String = ViewBag.dt2;
            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                per = PESA2(Date1String, Date2String);
            }

            switch (sortOrder)
            {

                case "Day":
                    per = per.OrderBy(s => s.Day);
                    break;

                default:
                    per = per.OrderBy(s => s.Day);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;
            return View(per.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Unlock()
        {
            return View();
        }
        public ActionResult DownloadPdf()
        {

            return new Rotativa.ActionAsPdf("Index");
        }
    }
}