﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MkibaBI.Reporting
{
    public partial class Registrations : System.Web.UI.Page
    {
        public string id = "";
        public string id2 = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["From"];
            id2 = Request.QueryString["To"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                Registration report = new Registration();
                report.Parameters["FDate"].Value = id;
                report.Parameters["TDate"].Value = id2;
                report.Parameters["Company"].Value = System.Web.HttpContext.Current.Session["safe"].ToString().Replace("OLD", "");
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}